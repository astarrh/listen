from django.db import models
from .choices import INTEREST_LEVELS, RESPONSES

from .managers import PersonSearchManager


class Quality(models.Model):
    label = models.CharField(max_length=128)


class Issue(models.Model):
    label = models.CharField(max_length=128)


class Tag(models.Model):
    label = models.CharField(max_length=128)


class Concern(models.Model):
    label = models.CharField(max_length=128)
    intensity = models.IntegerField(choices=INTEREST_LEVELS)


class Residence(models.Model):
    district = models.ForeignKey("Project.District", on_delete=models.SET_NULL, blank=True, null=True)
    address = models.CharField(max_length=128)
    city = models.CharField(max_length=64)
    state = models.CharField(max_length=24)
    postal_code = models.CharField(max_length=24)


class Person(models.Model):
    name = models.CharField(max_length=64)
    email = models.EmailField(null=True, blank=True)
    phone = models.CharField(max_length=24, null=True, blank=True)
    residence = models.ForeignKey(Residence, null=True, blank=True, on_delete=models.SET_NULL)
    district = models.ForeignKey("Project.District", on_delete=models.SET_NULL, blank=True, null=True)
    projects = models.ManyToManyField("Project.Project")

    objects = PersonSearchManager()


class Log(models.Model):
    user = models.ForeignKey('Home.ListenUser', on_delete=models.PROTECT)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    notes = models.TextField(blank=True)
    hours = models.IntegerField(blank=True, null=True)
    dollars = models.IntegerField(blank=True, null=True)
    project = models.ForeignKey("Project.Project", blank=True, null=True, on_delete=models.PROTECT)
    event = models.ForeignKey("Project.Event", blank=True, null=True, on_delete=models.PROTECT)
    concern = models.ForeignKey(Concern, models.PROTECT, blank=True, null=True)
    quality = models.ForeignKey(Quality, models.PROTECT, blank=True, null=True)
    old_address = models.ForeignKey(Residence, on_delete=models.PROTECT, null=True, blank=True, related_name="address_change_from")
    new_address = models.ForeignKey(Residence, on_delete=models.PROTECT, null=True, blank=True, related_name="address_change_to")
    response = models.IntegerField(choices=RESPONSES, blank=True, null=True)
