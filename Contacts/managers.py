import operator
from functools import reduce

from django.db.models import Q, Manager


class PersonSearchManager(Manager):

    def search(self, data):
        params = {}

        if data.get('name'):
            params['name__icontains'] = data.get('name')
        if data.get('phone'):
            params['phone__icontains'] = data.get('phone')
        if data.get('email'):
            params['email__icontains'] = data.get('email')
        if data.get('address'):
            params['residence__address__icontains'] = data.get('address')
        if data.get('project'):
            params['project__id'] = data.get('project')

        return self.model.objects.filter(**params)
