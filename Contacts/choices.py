UNASSESSABLE = 0
INTERESTED = 1
FAMILIAR = 2
PASSIONATE = 3

INTEREST_LEVELS = (
    ("Unassessable", UNASSESSABLE),
    ("Interested", INTERESTED),
    ("Passionate", PASSIONATE),
    ("Familiar", FAMILIAR)
)

NONE = 0
NEGATIVE = 1
NEUTRAL = 2
POSITIVE = 3

RESPONSES = (
    ("None", NONE),
    ("Negative", NEGATIVE),
    ("Neutral", NEUTRAL),
    ("Positive", POSITIVE)
)
