from django.views.generic import TemplateView
from django.contrib.auth.mixins import UserPassesTestMixin

from Home.mixins import LanguageMixin
from Project.models import Project


class ContactsView(LanguageMixin, TemplateView, UserPassesTestMixin):
    template_name = "contacts.html"
    app_name = 'contacts'

    def test_func(self):
        return self.request.user.is_admin

    def projects(self):
        return Project.objects.all()


class CommunityPortalView(LanguageMixin, TemplateView, UserPassesTestMixin):
    template_name = "portal.html"

    def test_func(self):
        return self.request.user.is_admin

    def projects(self):
        return Project.objects.all()
