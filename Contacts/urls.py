from django.urls import path

from Contacts import views, actions

urlpatterns = [
    # Views
    path('', views.ContactsView.as_view(), name='contacts'),
    path('portal/', views.CommunityPortalView.as_view(), name="portal"),

    # Actions
    path('manage_contact/', actions.manage_contact, name='manage_contact'),
    path('search_people/', actions.search_people, name='search_people')
]
