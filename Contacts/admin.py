from django.contrib import admin

from .models import Person, Residence, Log

admin.site.register(Log)
admin.site.register(Residence)
admin.site.register(Person)
