from json import loads as unpack
from tools import request_processors as rp

from django.template.loader import render_to_string

from django.http import JsonResponse

from .models import Person, Residence, Log


def manage_contact(request):
    allowed, response = rp.check_if_method_is_post(request)
    if allowed:
        request_data = unpack(request.body)

        residence, _ = Residence.objects.get_or_create(
            address=request_data.pop('address'),
            city='Shafter',
            state='CA',
            postal_code='93263'
        )
        person = Person.objects.create(**request_data)
        person.residence = residence

        Log.objects.create(user=request.user, person=person, new_address=residence)

        rp.set_successful_response_for_object(person)

    return JsonResponse(response)


def search_people(request):
    response = {'success': False}
    if request.method != "GET":
        return JsonResponse(response)

    string = ""
    results = Person.objects.search(request.GET.dict())
    for result in results:
        string += render_to_string('contact_card.html', {'object': result})

    response['success'] = True
    response['html'] = string
    return JsonResponse(response)
