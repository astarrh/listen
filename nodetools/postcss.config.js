// https://stackoverflow.com/questions/63392426/how-to-use-tailwindcss-with-django
// // instructions are missing "npm init -y " to create the packages.json file on step 1 just before installing packages.
// Actual packages to install: npm install tailwindcss@latest postcss@latest postcss-cli@latest autoprefixer@latest

module.exports = {
    plugins: [
        require('tailwindcss'),
        require('autoprefixer')
    ]
}

