module.exports = {
  purge:
      [
      '../Survey/templates/*.html',
      '../Home/templates/*.html',
      '../Analyze/templates/*.html',
      '../Blog/templates/*.html',
      '../Project/templates/*.html',
      '../Contacts/templates/*.html',
      '../Metrics/templates/*.html',
      '../frontend/src/*.vue',
      '../frontend/src/**/*.vue',
      '../frontend/src/**/**/*.vue',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
        'body': ['Karla', 'sans-serif'], // Ensure fonts with spaces have " " surrounding it.
        'title': ["Gowun Batang", 'serif']
    },
    extend: {},
  },
  variants: {
    extend: {
        grayscale: ['hover'],
        textColor: ['visited'],
    },
  },
  plugins: [
    require('@vueform/slider/tailwind'),
  ],
}
