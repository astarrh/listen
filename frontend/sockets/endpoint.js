let loc = window.location
let wsStart = location.protocol === 'https:' ? 'wss://' : 'ws://'
let endpoint = wsStart + loc.host + ":8001" + loc.pathname

export {endpoint}
