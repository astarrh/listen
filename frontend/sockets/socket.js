import {ref, watch} from 'vue'
import {endpoint} from "./endpoint";

const user = ref({})
let userMessage = ref("")
let socket = ref(buildSocket())
const results = ref({})
const errors = ref({})

let timeout = 500
let socketIsOpen = ref(false)

function setUser(userObj) {
  if (results.value.success) {
      user.value = userObj
  } else {
      user.value = null
  }
}

function buildSocket() {
    const newSocket = new WebSocket(endpoint)

    newSocket.onopen = function() {
        socketIsOpen.value = true
        userMessage.value = ""
        timeout = 500
    }

    newSocket.onmessage = function(e) {
        results.value = JSON.parse(e.data)
        if (results.value.success === false) {
            errors.value = results.value.errors
            console.log(errors.value)
        }
        if (results.value.operation === 'set_user') {
            setUser(results.value.object)
        }
    }

    newSocket.onerror = function () {
        userMessage.value = "We're having trouble connecting you to our servers. It's possible your internet has gotten disconnected. Your responses are not being saved."
    }

    newSocket.onclose = function () {
        socketIsOpen.value = false
        timeout = timeout * 2
        setTimeout(function () {
            userMessage.value = "Reconnecting..."
            socket.value = null
            socket.value = buildSocket();
        }, timeout);
    }
    return newSocket
}

let data = []
function sendData(chunk) {
    if (socketIsOpen.value) {
        socket.value.send(chunk)
    }
    else {
        data.push(chunk)
    }
}

watch(socketIsOpen, (opened) => {
    if (opened && data.length) {
        for (const chunk of data) {
            sendData(chunk)
        }
    }
})

export default {
    install: (app) => {
        const output = {
            send: sendData,
            data: {
                message: userMessage,
                results: results,
                errors: errors,
                user: user
            }
        }
        app.config.globalProperties.$socket = socket
        app.provide('socket', output)
    }
}