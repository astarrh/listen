import { createApp } from 'vue'
import AddResponse from "./AddResponse.vue";
import { createGettext } from "@jshmrtn/vue3-gettext";
import translations from "./translations/translations.json";

const gettext = createGettext({
  availableLanguages: {
    es: "Spanish",
  },
  defaultLanguage: "en_US",
  translations,
});


const surveyApp = createApp(AddResponse)

// Invoke with v-focus
surveyApp.directive('focus', {
  // When the bound element is mounted into the DOM...
  mounted(el) {
    // Focus the element
    el.focus()
  }
})

// surveyApp.use(socket)
surveyApp.use(gettext);

surveyApp.mount('#addResponse')
