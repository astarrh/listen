msgid ""
msgstr ""
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: easygettext\n"
"Project-Id-Version: \n"

#: ./src/TakeSurvey.vue:63
msgid "As promised, your responses will remain 100% anonymous."
msgstr "Como fue prometido, sus respuestas permanecerán 100% anónimas."

#: ./src/TakeSurvey.vue:32
msgid "Begin Survey"
msgstr "Comenzar encuesta"

#: ./src/components/AuthForm.vue:10
#: ./src/components/AuthForm.vue:26
msgid "Email or Phone"
msgstr "Email o número de teléfono:"

#: ./src/TakeSurvey.vue:81
msgid "Finish"
msgstr "Terminar"

#: ./src/components/SurveyFields/BooleanInput.vue:18
#: ./src/components/SurveyFields/RangeInput.vue:29
#: ./src/components/SurveyFields/SelectInput.vue:24
#: ./src/components/SurveyFields/TextInput.vue:12
msgid "Go Back"
msgstr "Regresar"

#: ./src/TakeSurvey.vue:23
msgid "If you do not recognize this information as yours, please"
msgstr "Si no reconoce esta información como suya, por favor"

#: ./src/TakeSurvey.vue:69
msgid "If you haven't yet, please be sure to complete the other surveys on your dashboard. Your participation is incredibly helpful to everyone who is working to make our city the best place it can be! Also, please consider sharing this website with some fellow Shafter people and inviting them to participate as well. We need every last person's help!"
msgstr "Si aún no lo ha hecho, por favor, asegúrese de completar las demás encuestas en su panel de información. ¡Su participación es sumamente útil para todos los que están trabajando para hacer de nuestra ciudad el mejor lugar donde se puede estar!"

#: ./src/TakeSurvey.vue:14
msgid "If you'd like our system to remember your answers so you won't lose your progress if you get interrupted, please sign up or log in to your Listen To Shafter profile:"
msgstr "Si desea que nuestro sistema recuerde sus respuestas para que no pierda su progreso si lo interrumpen, regístrese o inicie sesión en su perfil de Listen To Shafter:"

#: ./src/TakeSurvey.vue:66
msgid "Listen To Shafter is dedicated to learning about the people who live and work here. If you have the means, please consider sponsoring us with a small monthly donation. Your assistance will help us build a more complete picture of our community by giving a voice to more of your neighbors!"
msgstr "Listen To Shafter es una organización para el beneficio de la comunidad que se dedica a aprender sobre las personas que viven y trabajan en nuestra comunidad. Si usted tiene los medios, por favor, considere patrocinarnos con una pequeña donación mensual. ¡Con su ayuda podremos construir una imagen más completa de nuestra comunidad para que más vecinos se hagan escuchar!"

#: ./src/components/AuthForm.vue:3
#: ./src/components/AuthForm.vue:18
msgid "Log In"
msgstr "Iniciar Sesión"

#: ./src/TakeSurvey.vue:25
msgid "log out"
msgstr "cerrar sesión"

#: ./src/components/AuthForm.vue:14
#: ./src/components/AuthForm.vue:30
msgid "Password:"
msgstr "Contraseña"

#: ./src/components/SurveyFields/BooleanInput.vue:22
#: ./src/components/SurveyFields/RangeInput.vue:33
#: ./src/components/SurveyFields/SelectInput.vue:28
#: ./src/components/SurveyFields/TextInput.vue:16
msgid "Proceed"
msgstr "Continuar"

#: ./src/components/AuthForm.vue:34
msgid "Repeat Password:"
msgstr "Repite la contraseña"

#: ./src/components/AuthForm.vue:4
#: ./src/components/AuthForm.vue:38
msgid "Sign Up"
msgstr "Inscribirse"

#: ./src/TakeSurvey.vue:7
msgid "Thank You For Participating"
msgstr "Gracias por participar"

#: ./src/TakeSurvey.vue:59
msgid "Thanks again for taking part in this study!"
msgstr "¡Gracias de nuevo por participar en este estudio!"

#: ./src/TakeSurvey.vue:20
msgid "You are currently logged in with the following account information:"
msgstr "Ha iniciado sesión con la siguiente información de cuenta:"

#: ./src/TakeSurvey.vue:12
msgid "Your responses are 100% anonymous. We are an independent non-profit and are not affiliated with the City of Shafter in any way."
msgstr "Sus respuestas son 100% anónimas. Somos una organización independiente sin fines de lucro y no estamos afiliados con la ciudad de Shafter de ningún modo."
