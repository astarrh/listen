import { createApp } from 'vue'
import EditSurvey from './EditSurvey.vue'
// import socket from "../plugins/socket";

const app = createApp(EditSurvey)

// Invoke with v-focus
app.directive('focus', {
  // When the bound element is mounted into the DOM...
  mounted(el) {
    // Focus the element
    el.focus()
  }
})

// app.use(socket)


app.mount('#builderApp')
