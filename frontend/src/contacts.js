import { createApp } from 'vue'
import Contacts from './Contacts.vue'

const contactsApp = createApp(Contacts)

// Invoke with v-focus
contactsApp.directive('focus', {
  // When the bound element is mounted into the DOM...
  mounted(el) {
    // Focus the element
    el.focus()
  }
})


contactsApp.mount('#contactsApp')
