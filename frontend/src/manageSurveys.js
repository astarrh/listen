import { createApp } from 'vue'
import ManageSurvey from './ManageSurveys.vue'

const app = createApp(ManageSurvey)

// Invoke with v-focus
app.directive('focus', {
  // When the bound element is mounted into the DOM...
  mounted(el) {
    // Focus the element
    el.focus()
  }
})


app.mount('#managerApp')
