import axios from "axios";

// https://stackoverflow.com/questions/48001813/best-way-to-config-global-headers-for-get-post-patch-in-vuejs

var siteApi = axios.create({
  xsrfCookieName: window.rootData.csrfToken,
  xsrfHeaderName: "X-CSRFTOKEN",
});

export default siteApi

