from django import template

from Analyze.models import Researcher
from Survey.models import Question, TextAnswer

register = template.Library()

@register.simple_tag
def has_available_answer(question_id, user):
    question = Question.objects.get(id=question_id)
    answer = TextAnswer.items.next_available_for_review_per_user(question, Researcher.objects.get(user=user))
    return True if answer else False