from django.db.models import Manager, Avg


class ResolutionManager(Manager):

    def current_confidence_multiplier(self, question):
        value = 30
        current_set = self.model.objects.filter(response__question=question)
        if current_set.count() > 14:
            value = round(current_set.aggregate(Avg('confidence')) * 100)
        return value

