from django.http import HttpResponseRedirect
from django.db.models import Count
from django.shortcuts import redirect
from django.views.generic import TemplateView
from django.contrib.auth.mixins import UserPassesTestMixin

from Survey.models import Questionnaire, Question, TextAnswer
from .models import Datum, Note, Researcher, Post, Trace


class SurveySelectionView(UserPassesTestMixin, TemplateView):
    template_name = 'select_survey.html'

    def test_func(self):
        return self.request.user.is_researcher

    def surveys(self):
        researcher = Researcher.objects.get(user=self.request.user)
        print(researcher.eligible_for)
        return researcher.eligible_for.all()


class QuestionSelectionView(UserPassesTestMixin, TemplateView):
    template_name = 'select_question.html'

    def test_func(self):
        return self.request.user.is_researcher

    def survey(self):
        return Questionnaire.objects.get(id=self.kwargs.get('survey_id'))


class EvaluationView(UserPassesTestMixin, TemplateView):
    template_name = 'evaluation.html'

    def test_func(self):
        return self.request.user.is_researcher

    def dispatch(self, request, *args, **kwargs):
        if not self.answer():
            return redirect('Analyze:select_question', survey_id=self.kwargs.get('survey_id'))
        return super(EvaluationView, self).dispatch(request, *args, **kwargs)

    def question(self):
        return Question.objects.get(id=self.kwargs.get('question_id'))

    def answer(self):
        answer = TextAnswer.items.next_available_for_review_per_user(
            self.question(), Researcher.objects.get(user=self.request.user)
        )
        return answer

    def available_codes(self):
        return Datum.objects.filter(question=self.question()).annotate(count=Count('evaluations')).order_by('-count')

    def available_notes(self):
        return Note.objects.filter(question=self.question()).annotate(count=Count('evaluations')).order_by('-count')

    def discussion(self):
        posts = Post.objects.filter(question=self.question()).order_by('-moment')
        print(posts)
        html = ""
        for post in posts:
            html += post.html()
        return html


class GeneralActivityView(UserPassesTestMixin, TemplateView):
    template_name = 'records.html'

    def test_func(self):
        return self.request.user.is_admin

    def traces(self):
        return Trace.objects.all().order_by('-moment')[:100]


class UserActivityView(UserPassesTestMixin, TemplateView):
    template_name = 'records.html'

    def test_func(self):
        return self.request.user.is_admin

    def traces(self):
        return Trace.objects.filter(researcher=self.kwargs.get('slug')).order_by('-moment')[:100]


class AnswerActivityView(UserPassesTestMixin, TemplateView):
    template_name = 'records.html'

    def test_func(self):
        return self.request.user.is_admin

    def traces(self):
        return Trace.objects.filter(answer=self.kwargs.get('slug')).order_by('-moment')[:100]
