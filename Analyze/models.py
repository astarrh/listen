from datetime import timedelta, datetime
from django.db import models
from .managers import ResolutionManager


RESEARCHER_WEIGHTS = {
    1 : 3,
    2 : 14,
    3 : 23,
    4 : 25,
    5 : 26.7,
    6 : 200 # Master Account
}

RESEARCHER_THRESHOLDS = {
    2: 25,
    3: 100,
    4: 500,
    5: 1000,
    6: 10000, # Safety Value (Not actually included in loop).
}


class Researcher(models.Model):
    """ User with a role of researcher """
    user = models.ForeignKey('Home.ListenUser', on_delete=models.CASCADE)
    name = models.CharField(max_length=48, default='anonymous')
    eligible_for = models.ManyToManyField('Survey.Questionnaire')
    base_competency = models.IntegerField(default=1) # Initial competency status. Should be 1-5.

    def correct_evaluations(self):
        return self.evaluation_set.filter(correct=True)

    def incorrect_evaluations(self):
        return self.evaluation_set.filter(correct=False)

    def total_evaluations(self):
        return self.evaluation_set.all().count()

    def total_rated_evaluation(self):
        return self.correct_evaluations() + self.incorrect_evaluations()

    def calculate_recent_success_ratio(self, qty):
        recent_set = self.evaluation_set.filter(correct__isnull=False).order_by('-id')[:qty]
        if recent_set:
            correct_in_recent_set = self.evaluation_set.filter(id__in=recent_set, correct=True)
            result = correct_in_recent_set.count() / len(recent_set)
        else:
            result = 0
        return result

    def calculate_current_competency(self):
        evaluated_competency = base_competency = self.base_competency
        for x in range (5, 1, -1):
            threshold = RESEARCHER_THRESHOLDS[x]
            if self.total_evaluations() >= threshold and self.calculate_recent_success_ratio(threshold) >= .9:
                evaluated_competency = x if x >= base_competency else base_competency
        return evaluated_competency

    def current_weight(self):
        return RESEARCHER_WEIGHTS[self.calculate_current_competency()]


class Datum(models.Model):
    """ An option that a user has associated with a question. """
    question = models.ForeignKey('Survey.Question', on_delete=models.CASCADE)
    label = models.CharField(max_length=64)
    upper = models.CharField(max_length=64)


class Note(models.Model):
    """ Further Information related to a Datum. """
    question = models.ForeignKey('Survey.Question', on_delete=models.CASCADE)
    label = models.CharField(max_length=64)
    upper = models.CharField(max_length=64)


class Evaluation(models.Model):
    """ Coordinating object linking researchers to responses that they have evaluated and assigning a weight value based
        on the researcher's rating at the time of evaluation. """
    researcher = models.ForeignKey(Researcher, on_delete=models.CASCADE)
    response = models.ForeignKey('Survey.TextAnswer', on_delete=models.CASCADE, related_name='evaluations')
    datum = models.ManyToManyField(Datum, related_name='evaluations')
    # data_string = models.CharField(max_length=64)
    correct = models.BooleanField(blank=True, null=True)
    notes = models.ManyToManyField(Note, related_name='evaluations')
    weight = models.IntegerField(blank=True, null=True)
    timestamp = models.DateTimeField()
    skipped = models.BooleanField(default=False)


class Resolution(models.Model):
    """ An independent record recording an accepted evaluation based on weight-appreciative researcher consensus. """
    response = models.ForeignKey('Survey.TextAnswer', on_delete=models.CASCADE, related_name='resolution')
    datum = models.ManyToManyField(Datum, related_name='resolutions')
    points_for = models.IntegerField() # Represents the point value of the resolution based on total evaluation weight.
    points_against = models.IntegerField()
    certainty = models.IntegerField()
    false_count = models.IntegerField()
    confidence = models.FloatField() # 1 - (Total Points for Rejected Answers / Own Points)

    calculations = ResolutionManager()
    objects = models.Manager()


class Post(models.Model):
    """ A comment or description of activity pertaining to a survey question or answer"""
    researcher = models.ForeignKey(Researcher, on_delete=models.SET_NULL, null=True, blank=True, related_name='posts')
    question = models.ForeignKey('Survey.Question', on_delete=models.CASCADE, related_name='discussions', null=True)
    answer = models.ForeignKey('Survey.TextAnswer', on_delete=models.CASCADE, related_name='discussions', null=True)
    moment = models.DateTimeField(auto_now=True)
    action = models.CharField(max_length=748)
    comment = models.TextField(blank=True, null=True)

    def html(self):
        if self.moment.date() == datetime.today().date():
            time = "Today"
        elif self.moment.date() == datetime.today().date() - timedelta(days=1):
            time = "Yesterday"
        else:
            time = "on " + self.moment.strftime('%A, %b %d')

        html = "<div class='post-frame'>"
        html += "<span class='post-head'>{} {} {}</span>".format(
            self.researcher.name,
            self.action,
            time
        )
        if self.comment:
            html += "<p class='post-text'>{}</p>".format(self.comment)
        html += "</div>"
        return html


class Trace(models.Model):
    """ Records Evaluation Activity for Website """
    researcher = models.ForeignKey(Researcher, on_delete=models.SET_NULL, null=True, blank=True, related_name='traces')
    question = models.ForeignKey('Survey.Question', on_delete=models.SET_NULL, related_name='traces', null=True)
    answer = models.ForeignKey('Survey.TextAnswer', on_delete=models.SET_NULL, related_name='traces', null=True)
    evaluation = models.ForeignKey(Evaluation, on_delete=models.SET_NULL, related_name='traces', null=True)
    resolution = models.ForeignKey(Resolution, on_delete=models.SET_NULL, related_name='traces', null=True)
    data = models.ManyToManyField(Datum, related_name='traces')
    rscore = models.IntegerField()
    conf_x = models.IntegerField()
    moment = models.DateTimeField(auto_now_add=True)
    best_answer = models.CharField(max_length=128)
    best_answer_value = models.IntegerField()
    other_values = models.CharField(max_length=128)
