from django.urls import path

from Analyze import views, actions

urlpatterns = [
    path('evaluate/select_survey', views.SurveySelectionView.as_view(), name='select_survey'),
    path('evaluate/<int:survey_id>', views.QuestionSelectionView.as_view(), name='select_question'),
    path('evaluate/<int:survey_id>/<int:question_id>', views.EvaluationView.as_view(), name='evaluate'),

    path('add_code/', actions.create_code),
    path('add_note/', actions.create_note),
    path('add_comment/', actions.add_comment),
    path('add_researcher/', actions.add_researcher),
    path('delete_tag/', actions.delete_tag),
    path('merge_tags/', actions.merge_tags),
    path('select_discussion/', actions.select_discussion),
    path('submit_evaluation/', actions.save_evaluation),
    path('records/user/<slug:slug>', views.UserActivityView.as_view(), name='user-records'),
    path('records/answer/<slug:slug>', views.AnswerActivityView.as_view(), name='answer-records'),
    path('records/', views.GeneralActivityView.as_view(), name='general-records'),
]
