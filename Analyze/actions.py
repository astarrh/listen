import operator
from django.utils import timezone
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist

from Home.models import ListenUser
from Survey.models import Question, TextAnswer
from .models import Evaluation, Researcher, Datum, Note, Resolution, Post, Trace


def add_researcher(request):
    if request.method != "POST" or not request.user.is_admin:
        return JsonResponse({'success': False})

    Researcher.objects.create(
        user=ListenUser.objects.get(username=request.POST.get('account')),
        name=request.POST.get('name'),
        base_competency=request.POST.get('base_comp')
    )

    return JsonResponse({'success': True})


def create_code(request):
    response = {'success': False}
    if request.method != "POST":
        return JsonResponse(response)

    question = int(request.POST.get('question'))
    text = request.POST.get('text')
    upper = text.upper()
    existing = Datum.objects.filter(upper=upper, question=question)

    if existing.exists():
        result = existing.first().id
        created = False
    else:
        new_datum = Datum.objects.create(question_id=question, label=text, upper=upper)
        result = new_datum.id
        created = True

    response = {'success': True, 'id': result, 'created': created}

    return JsonResponse(response)


def create_note(request):
    response = {'success': False}
    if request.method != "POST":
        return JsonResponse(response)

    question = int(request.POST.get('question'))
    text = request.POST.get('text')
    upper = text.upper()

    # Remove This (One Time Use)
    for datum in Note.objects.all():
        datum.upper = datum.label.upper()
        datum.save()

    existing = Note.objects.filter(upper=upper)

    if existing.exists():
        result = existing.first().id
        created = False
    else:
        new_note = Note.objects.create(question_id=question, label=text, upper=upper)
        result = new_note.id
        created = True

    response = {'success': True, 'id': result, 'created': created}

    return JsonResponse(response)


def update_answer(answer):
    resolution = None
    eval_objs = Evaluation.objects.filter(response_id=answer, skipped=False).prefetch_related('datum')

    results = {}

    for obj in eval_objs:
        # Get a string value representing the full list of codes assigned in the evaluation.
        data = list(obj.datum.all().order_by('id').values_list('id', flat=True))
        if str(data) in results:
            results[str(data)] += obj.weight
        else:
            results[str(data)] = obj.weight

    best_answer = max(results.items(), key=operator.itemgetter(1))[0]
    best_answer_value = results.pop(best_answer)
    other_values = sum(results.values())

    print(best_answer_value)
    print(other_values)

    if best_answer_value and best_answer_value > 500 and best_answer_value > other_values:
        print("Creating Resolution")
        Resolution.objects.filter(response_id=answer).delete()

        confidence_interval = 1 - (other_values / best_answer_value)

        resolution = Resolution.objects.create(
            response_id=answer,
            points_for=best_answer_value,
            points_against=other_values,
            certainty=best_answer_value - other_values,
            confidence=confidence_interval,
            false_count=len(results)
        )

        print(eval(best_answer))
        resolution.datum.add(*eval(best_answer))

    return resolution, best_answer, best_answer_value, other_values


def save_evaluation(request):
    response = {'success': False}
    resolution = None

    if request.method != "POST":
        return JsonResponse(response)
    print(request.POST)

    researcher = Researcher.objects.get(user=request.user)
    answer = int(request.POST.get('answer'))
    codes = list(map(int, request.POST.getlist('codes[]')))
    notes = list(map(int, request.POST.getlist('notes[]')))
    skipped = True if request.POST.get('skipped') == 'true' else False

    Evaluation.objects.filter(researcher=researcher, response=answer).delete()

    evaluation = Evaluation.objects.create(
        researcher=researcher,
        response_id=answer,
        timestamp=timezone.now(),
        skipped=skipped
    )

    if not skipped:
        evaluation.datum.add(*codes)
        evaluation.notes.add(*notes)

        # Calculate and set weight
        question = evaluation.response.question
        rscore = researcher.current_weight()
        aconst = .7
        bconst = 4.4
        conf_x = Resolution.calculations.current_confidence_multiplier(question)

        evaluation.weight = round((conf_x * aconst) + rscore + (rscore * (conf_x / bconst)))  # ???
        evaluation.save()

        resolution, best_answer, best_answer_value, other_values = update_answer(answer)

        trace = Trace.objects.create(
            researcher=researcher,
            question=question,
            answer_id=answer,
            evaluation=evaluation,
            resolution=resolution,
            rscore=rscore,
            conf_x=conf_x,
            best_answer=best_answer,
            best_answer_value=best_answer_value,
            other_values=other_values,
        )

        trace.data.add(*codes)

    response = {'success': True}

    return JsonResponse(response)


def add_comment(request):
    response = {'success': False}
    if request.method != "POST" or not request.user.is_researcher():
        return JsonResponse(response)

    researcher = Researcher.objects.get(user=request.user)
    action = 'commented'
    posts = None
    mode = request.POST.get('mode')
    index = int(request.POST.get('index'))
    text = request.POST.get('text')

    if mode == "forQuestion":
        question = TextAnswer.objects.get(id=index).question
        Post.objects.create(researcher=researcher, question=question, action=action, comment=text)
        posts = Post.objects.filter(question=question).order_by('-moment')
        response = {'success': True}
    elif mode == "forAnswer":
        answer = TextAnswer.objects.get(id=index)
        Post.objects.create(researcher=researcher, answer=answer, action=action, comment=text)
        posts = Post.objects.filter(answer=answer).order_by('-moment')
        response = {'success': True}

    html = ""
    for post in posts:
        html += post.html()
    response['html'] = html

    return JsonResponse(response)


def select_discussion(request):
    response = {'success': False}
    if request.method != "POST" or not request.user.is_researcher():
        return JsonResponse(response)

    posts = None
    mode = request.POST.get('mode')
    index = request.POST.get('index')

    if mode == "forQuestion":
        question = Question.objects.get(id=index)
        posts = Post.objects.filter(question=question).order_by('-moment')
        response = {'success': True}
    elif mode == "forAnswer":
        answer = TextAnswer.objects.get(id=index)
        posts = Post.objects.filter(answer=answer).order_by('-moment')
        response = {'success': True}
    html = ""

    for post in posts:
        html += post.html()
    response['html'] = html

    return JsonResponse(response)


def delete_tag(request):
    response = {'success': False}
    if request.method != "POST" or not request.user.is_admin:
        return JsonResponse(response)

    type = request.POST.get('type')
    pk = request.POST.get('id')
    if type == "codes":
        datum = Datum.objects.get(id=pk)
        if not Evaluation.objects.filter(datum=datum).exists():
            question = datum.question
            action = 'deleted code: ""'.format(datum.label)
            datum.delete()
            response = {'success': True}
        else:
            response['message'] = "This code has evaluations associated with it. Please merge it into another code."
            return JsonResponse(response)
    else:
        note = Note.objects.get(id=pk)

        question = note.question
        action = 'deleted note: ""'.format(note.label)
        note.delete()
        response = {'success': True}

    researcher = Researcher.objects.get(user=request.user)
    Post.objects.create(researcher=researcher, question=question, moment=timezone.now(), action=action)

    return JsonResponse(response)


def merge_tags(request):
    response = {'success': False}
    if request.method != "POST" or not request.user.is_admin:
        return JsonResponse(response)

    type = request.POST.get('type')
    primary_pk = request.POST.get('primary')
    secondary_pk = request.POST.get('secondary')

    if type == 'codes':
        primary = Datum.objects.get(id=primary_pk)
        secondary = Datum.objects.get(id=secondary_pk)
        question = primary.question
        action = 'merged code: "{}" into code: "{}"'.format(secondary.label, primary.label)
        adjusted_evaluations = secondary.evaluations.all()
        for evaluation in adjusted_evaluations:
            evaluation.datum.add(primary)
            resolution = evaluation.response.resolution.all()
            if resolution:
                resolution = resolution.first()
                answer = resolution.response
                resolution.delete()
                update_answer(answer)
        secondary.delete()
    else:
        primary = Note.objects.get(id=primary_pk)
        secondary = Note.objects.get(id=secondary_pk)
        question = primary.question
        action = 'merged note: "{}" into note: "{}"'.format(secondary.label, primary.label)
        adjusted_evaluations = primary.evaluations.all()
        for evaluation in adjusted_evaluations:
            evaluation.notes.add(secondary)
        secondary.delete()

    researcher = Researcher.objects.get(user=request.user)
    Post.objects.create(researcher=researcher, question=question, moment=timezone.now(), action=action)

    return JsonResponse(response)