from django.contrib import admin
from Analyze.models import Researcher, Evaluation, Resolution

for model in [Resolution, Researcher, Evaluation]:
    admin.site.register(model)