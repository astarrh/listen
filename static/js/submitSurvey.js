$( document ).ready(function() {

   $('.next').click(function () {
      var $form = $('.carousel-item.active').find($('form'));
      console.log($form);
      if ($form.length === 0) {
          $('#nextBtn').click();
      } else {
          submitResponse($form);
      }
   });

  $('.prev').click(function () {
      $('#prevBtn').click();
   });

});

function submitResponse($form) {
    // handleInputs();
    console.log($form.serialize());
    $('#nextBtn').click();
    $.ajax({
        url: "/record_response/" + $form.data('question_id') + "/",
        type: "POST",
        dataType: 'json',
        data: $form.serialize(),
        success: function (result) {
            if (result.success === true) {
                $form.attr('answer_id', result.answer_id);
            } else {
                $('.carousel-item.active').find($('.error')).show();
            }
        }
});
}

function handleInputs(){
    $('input[type=checkbox]').each(function() {
        if (!this.checked) {
            $(this).attr("value","0");
        }else{
            $(this).attr("value","1");
        }
    });
}