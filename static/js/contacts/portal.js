$(document).ready(function() {

    $('#name, #phone, #address, #email').on('input', function() {
        setTimeout(function() {
            const formData = $('#addPerson').serialize();
            $.ajax({
                   url: "/community/search_people/",
                   type: "GET",
                   dataType: 'json',
                   data: formData,
                   success: function(result) {
                        $('#contactFilterResults').html(result.html)
                   }
             });
        }, 500)

    });

});


$(document).on('click', '#contactSubmit', function(event) {
   event.preventDefault();
   const formData = $('#addPerson').serialize();
   $.ajax({
       url: "/community/add_person/",
       type: "POST",
       dataType: 'json',
       data: formData,
       success: function(result) {
            $('#contactFilterResults').html(result.html)
       }
   });
});