$(document).ready(function () {

    $('#addCode').keyup(function() {
       if ($(this).val()) {
        $('#addCodeBtn').removeClass('hidden')
       } else {
           $('#addCodeBtn').addClass('hidden')
       }
    });

    $('#addNote').keyup(function() {
       if ($(this).val()) {
        $('#addNoteBtn').removeClass('hidden')
       } else {
           $('#addNoteBtn').addClass('hidden')
       }
    });

    $('#addPostModal').on('shown.bs.modal', function() {
        $('#comment').trigger('focus');
    });


    $('#addCodeBtn').click(function() {
        var text = $('#addCode').val();
        if (text) {
            var question = $(this).data('question');

            $.ajax({
                url: "/analysis/add_code/",
                type: "POST",
                dataType: 'json',
                data: {
                    text: text,
                    question: question
                },
                success: function (result) {
                    if (result.created === true) {
                        $('#codes').append('<li class="list-group-item selected" data-id="' + result.id + '">' + text + '</li>');
                        $('#finishBtn').removeClass('disabled')
                    } else {
                        $('#codes').children().each(function(index) {
                            if ($(this).data('id') === result.id) {
                                $(this).addClass('selected')
                            }
                        })
                    }

                    $('#addCode').val('')
                }
            })
        }
    });


    $('#addNoteBtn').click(function() {
        var text = $('#addNote').val();
        if (text) {
            var question = $(this).data('question');

            $.ajax({
                url: "/analysis/add_note/",
                type: "POST",
                dataType: 'json',
                data: {
                    text: text,
                    question: question
                },
                success: function (result) {
                    if (result.created === true) {
                        $('#notes').append('<li class="list-group-item selected" data-id="' + result.id + '">' + text + '</li>');
                    } else {
                        $('#notes').children().each(function(index) {
                            if ($(this).data('id') === result.id) {
                                $(this).addClass('selected')
                            }
                        })
                    }
                    $('#addNote').val('')

                }
            })
        }
    });

    $('#postBtn').click(function(){
        var text = $('#comment').val();
        if (text) {
            var index = $(this).data('answer');
            var mode = $('#mode').val();
            $.ajax({
                url: "/analysis/add_comment/",
                type: "POST",
                dataType: 'json',
                data: {
                    text: text,
                    index: index,
                    mode: mode
                },
                success: function (result) {
                    $('#discussion').html(result.html)
                }
            });
        $('.select-discussion.active').removeClass('active');
        $('#'+mode).addClass('active')
        }
    });


    $(document).on('click', '.list-group-item', function() {
       if ($(this).hasClass('delete')) {
           var tagName = $(this).html();
          if (window.confirm("Delete Tag: " + tagName) === true) {
              deleteTag($(this))
          } else {
              return null
          }
       } else if ($(this).hasClass('merge')) {
           var tagName = $(this).html();
           if ($(this).hasClass('selected')) {
               $(this).removeClass('selected')
           } else {
               $(this).addClass('selected')
           }
           if ($(this).parent().attr('id') === 'codes') {
               $('#notes').children().removeClass('selected');
               if ($('.list-group-item.selected').length === 2) {
                   if (window.confirm("Merge Into Code: " + tagName) === true) {
                          mergeTags($(this))
                      } else {
                          return null
                      }
                }
           } else {
               $('#codes').children().removeClass('selected');
               if ($('.list-group-item.selected').length === 2) {
                  if (window.confirm("Merge Into Note: " + tagName) === true) {
                      mergeTags($(this))
                  } else {
                      return null
                  }
               }
           }
       } else if ($(this).hasClass('selected')) {
           $(this).removeClass('selected')
       } else {
           $(this).addClass('selected')
       }
       if ($(this).parent().attr('id') === 'codes') {
            if ($('#codes li').hasClass('selected')) {
                $('#finishBtn').removeClass('disabled')
            } else {
                $('#finishBtn').addClass('disabled')
            }
       }
    });


    $('#finishBtn').click(function() {

        $('#overlay').fadeIn();

        var answer = $(this).data('answer');

        var codes = [];
        $('#codes').children().each(function(ind, item) {
            if ($(this).hasClass('selected')) {
                codes.push($(this).data('id'))
            }
        });

        var notes = [];
        $('#notes').children().each(function(ind, item) {
            if ($(this).hasClass('selected')) {
                notes.push($(this).data('id'))
            }
        });

        $.ajax({
            url: '/analysis/submit_evaluation/',
            type:'POST',
            dataType: 'json',
            data : {
                answer: answer,
                codes: codes,
                notes: notes,
                skipped: false
            },
            success: function() {
                location.reload()
            }
        })
    });

    $('#skipBtn').click(function() {

        $.ajax({
            url: '/analysis/submit_evaluation/',
            type:'POST',
            dataType: 'json',
            data : {
                answer: $(this).data('answer'),
                codes: [],
                notes: [],
                skipped: true
            },
            success: function() {
                location.reload()
            }
        })
    });

    $('.select-discussion').click(function(){
        var mode = $(this).attr('id');
        var index = $(this).data('lookup');
         $.ajax({
            url: '/analysis/select_discussion/',
            type:'POST',
            dataType: 'json',
            data : {
                mode: mode,
                index: index,
            },
            success: function(result) {
                $('#discussion').html(result.html);
            }

        });
        $('.select-discussion.active').removeClass('active');
        $(this).addClass('active')
    });

    $('#removeToggle').click(function(){
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('#main').removeClass('delete');
            $('.list-group-item').removeClass('delete')
        } else {
            $(this).addClass('active');
            $('#mergeToggle').removeClass('active');
            $('#main').removeClass('merge').addClass('delete');
            $('.list-group-item').removeClass('merge').removeClass('selected').addClass('delete')
        }
    });

    $('#mergeToggle').click(function(){
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('#main').removeClass('merge');
            $('.list-group-item').removeClass('merge').removeClass('selected')
        } else {
            $(this).addClass('active');
            $('#deleteToggle').removeClass('active');
            $('#main').removeClass('delete').addClass('merge');
            $('.list-group-item').removeClass('delete').removeClass('selected').addClass('merge')
        }
    });

    function deleteTag($elem) {
        $.ajax({
            url: '/analysis/delete_tag/',
            type:'POST',
            dataType: 'json',
            data : {
                id: $elem.data('id'),
                type: $elem.parent().attr('id')
            },
            success: function(response) {
                if (response.success === true) {
                    $elem.remove()
                } else {
                    alert(response.message)
                }
            }
        })
    }

    function mergeTags($elem) {
        var $secondary;
        $('.list-group-item.selected').each(function(index, selected){
           if (!$(selected).is($elem)) {
               $secondary = $(selected);
           }
        });
        $.ajax({
            url: '/analysis/merge_tags/',
            type:'POST',
            dataType: 'json',
            data : {
                primary: $elem.data('id'),
                secondary: $secondary.data('id'),
                type: $elem.parent().attr('id')
            },
            success: function() {
                $secondary.remove();
                $('.list-group-item').removeClass('selected')
            }
        })
    }

});