$(document).ready(function () {

    $('#subscribe').click(function (e) {
        e.preventDefault();
        $(this).hide();
        $('#subscribeForm').fadeIn();
    });

    $('#subscribeConfirm').on('click', function(e) {
        e.preventDefault();
        var slug = $('#emailSignup').data('project');
        $.ajax({
            url: "/"+slug+"/subscribe/",
            type: "POST",
            dataType: 'json',
            data: {
                'name': $('#id_name').val(),
                'email': $('#id_email').val(),
            },
            success: function (result) {
                if (result.success === true) {
                    $('#subscribeForm').hide();
                    $('#prompt_email_signup').hide();
                    console.log(result);
                    if (result.existing_email === true) {
                       $('#responseExisting').fadeIn()
                    }
                    else if (result.existing_user === false) {
                        $('#createAccountForm').fadeIn()
                    } else {
                        $('#responseConfirm').fadeIn()
                    }
                } else {
                    alert(result.errors)
                }
            }
        })
    });

    $('#noThanks').on('click', function(e) {
        e.preventDefault();
        $('#createAccountForm').hide();
        $('#responseConfirm').fadeIn()
    });

    $('#newAccount').on('click', function(e) {
        e.preventDefault();
         $.ajax({
            url: "/quick_signup/",
            type: "POST",
            dataType: 'json',
            data: {
                'email': $('#id_email').val(),
                'password': $("#id_password").val()
            },
            success: function(result) {
                if (result.success === true) {
                    $('#createAccountForm').hide();
                    $('#responseConfirm').fadeIn()
                } else {
                    alert(result.error)
                }
            }
         })
    })

});