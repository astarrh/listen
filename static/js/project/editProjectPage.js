$(document).ready(function() {
   $('.moveUp').first().addClass('disabled');
   $('.moveDown').last().addClass('disabled');
});

$(document).on('click', '.sectionFrame.add', function(event) {
    var sectionID = $(event.target.parentElement).data('section');
    $('#createSectionModal').data('section', sectionID).modal('show');
    $('.activeSection').removeClass('activeSection');
    $(this).addClass('activeSection')
});

$(document).on('click', '#addSectionBtn', function(event){
    $.ajax({
        url: "/projects/add_new_project_section/",
        type: "POST",
        dataType: 'json',
        data: {
            'slug':$('#contentFrame').data('slug'),
            'config': $('#chooseConfiguration').val(),
            'title': $('#newSectionTitle').val(),
            'title_translation': $('#newSectionTitleTranslation').val(),
        },
        success: function (result) {
            console.log($('#contentEnd'));
            $('#contentEnd').before(result.string)
        }
    });

    $('#createSectionModal').modal('hide')

});

function arrangeSection(thisPosition, otherPosition, $elem) {
    $.ajax({
        url: "/projects/arrange_sections/",
        type: "POST",
        dataType: 'json',
        data: {
            'slug':$('#contentFrame').data('slug'),
            'positionA': thisPosition,
            'positionB': otherPosition,
        },
        success: function (result) {
            if (!otherPosition) {
                $elem.next().remove();
                $elem.remove()
            } else if (otherPosition > thisPosition) {
                var $hr = $elem.next();
                $hr.next().after($elem);
                $elem.before($hr);
            } else {
                var $hr = $elem.prev();
                $hr.prev().before($elem);
                $elem.after($hr);
            }
        }
    });
}

$(document).on('click', '.moveUp', function(event){
    if (!$(this).hasClass('disabled')) {
        var thisPosition = $(this).closest('.sectionFrame').data('position');
        var otherPosition = thisPosition - 1;
        arrangeSection(thisPosition, otherPosition, $(this).closest('.sectionFrame'))
    }
});

$(document).on('click', '.moveDown', function(event){
    if (!$(this).hasClass('disabled')) {
        var thisPosition = $(this).closest('.sectionFrame').data('position');
        var otherPosition = thisPosition + 1;
        arrangeSection(thisPosition, otherPosition, $(this).closest('.sectionFrame'))
    }
});

$(document).on('click', '.remove', function(event){
    var thisPosition = $(this).closest('.sectionFrame').data('position');
    var otherPosition = null;
    var r = confirm("Really delete this section?");
        if (r == true) {
         arrangeSection(thisPosition, otherPosition, $(this).closest('.sectionFrame'))
        }
});

$(document).on('click', '.addItem', function(event){
    var sectionID = $(event.target.parentElement).data('section');
    $('#createContentModal').data('section', sectionID).modal('show');
    $('.activeItem').removeClass('activeItem');
    $(this).addClass('activeItem')
});

$(document).on('click', '#addItemBtn', function(){
    var $modal = $('#createContentModal');
    var sectionID = $modal.data('section');
    var displayType = $('#chooseDisplay').val();
    var contentText = $('#newItemText').val();
    var translation = $('#newItemTranslation').val();

    $.ajax({
        url: '/projects/add_project_page_content/',
        type: 'POST',
        dataType: 'json',
        data: {
            'section': sectionID,
            'display_type': displayType,
            'text': contentText,
            'translation': translation
        },
        success: function (result) {
            if (result.success === true) {
                $('.activeItem').removeClass('activeItem').find('.sectionFrame').html(result.string);
            } else {
                alert('Error (Wrong Request Type)')
            }
        }
    });
    $modal.modal('hide');
});
