$(document).ready(function() {
   $('#langSwitch').click(function () {
       var location = $(this).data('location');

       $.ajax({
           url: "/change_language/",
           type: "POST",
           dataType: 'json',
           data: {'data': location},
           success: function (result) {
               window.location.reload()
           }
       })

   })
});
