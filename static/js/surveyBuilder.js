$(document).ready(function(){
   $( "#questionList" ).sortable({
       stop: function(event, ui) {
           set_survey_order(ui.item);
       }
   });

   $(document).on('click', '.card-body', function(){
       $(this).find('.question-text').prop('disabled', false);
       $(this).parent().find('.question-btns').fadeIn();
   });

   $(document).on('click', '.updateBtn', function(){
       var $btn = $(this);
       var $card = $(this).closest('.card');
       var questionnaireID = $('#surveyTitle').data('survey');
       var questionType = $card.find('.question-type').val();
       var questionFields = $(this).closest('.card-body').find('.question-row').find('.question-input').children();
       var translations = [];
       var questionText = [];
       questionFields.each(function(i, obj){
           var lang = $(obj).data('lang');
           var text = $(obj).val();
           if (lang === 0) {
               questionText = text
           } else {
               translations.push({
                   'lang': lang,
                   'text': text
               })
           }
       });

       var data = {
           'question': $card.attr('id').split('-')[1],
           'questionnaire': questionnaireID,
           'text': questionText,
           'response_format': questionType,
           'translations': JSON.stringify(translations)
       };

       var options = [];

       if (questionType === '3' || questionType === "5") {
           $card.find('.options').children().each(function() {
               if ($(this).hasClass('option')) {
                   return true;
               }
               options.push({
                   'text': $(this).val(),
                   'lang': $(this).data('lang')
               })
           });
           console.log(options);
           data.options = JSON.stringify(options);
           console.log(data)
       }

       $.ajax({
           url: "/update_survey/",
           type: "POST",
           dataType: 'json',
           data: data,
           success: function (result) {
               if (result.success === true) {
                   $btn.parent().fadeOut();
                   $card.attr('id', 'question-'+result.id)
               } else {
                   alert(result.message)
               }
           }
       });
   });

    $(document).on('click', '.deleteBtn', function() {
        var $card = $(this).closest('.card');
        var confirmed = confirm("Sure?");

        if (confirmed === true) {
            $.ajax({
                url: "/delete_question/",
                type: "POST",
                dataType: 'json',
                data: {'question': $card.attr('id').split('-')[1]},
                success: function (result) {
                    if (result.message === "success") {
                        $card.remove()
                    } else {
                        alert(result.message)
                    }
                }
            })
        }
    });

   $(document).on('change', '.question-type', function(){
        if($('option:selected',this).val() === '3' || $('option:selected',this).val() === '5') {
            $(this).next().show();
        } else {
            $(this).next().hide();
        }
    });

   $(document).on('click', '.optionAdd', function(){
       let earmark = this;
       var newOpt = $('#optInputNew').clone().attr('id', "").removeClass(('new'));
       $(newOpt).insertBefore(earmark);

       var langs =$(this).data('langs').split(',');
       $.each(langs, function(key, value){
           if(value.length) {
                var newOpt = $('#optInputNew').clone().attr('id', "").attr('data-lang', value).attr('style', 'display: none').removeClass(('new'));
                console.log(newOpt);
               $(newOpt).insertBefore(earmark);
           }
       })
   });

   $('#createNew').click(function(){

       $.ajax({
           url: "/add_new_question/",
           type: "POST",
           dataType: 'json',
           data: {survey: $('#surveyTitle').data('survey')},
           success: function (result) {
               if (result.message === "success") {
                   var $newCard = $('#new').clone();
                   $newCard.attr('id', "question-"+result.id);
                   $newCard.attr('data-index', result.position);
                   $('#questionList').append($newCard);
                   $newCard.slideDown()
               }
           }
       });
   });

   $('#saveBtn').click(function(){
        window.location.href = '/surveys/'
    });

   $(document).on('click', '.lang-btn', function(){
       var lang = $(this).attr('id').split('-')[1];
       $(this).closest('.card-body').find('.question-text').hide();
       $(this).closest('.card-body').find('.addOptionInput').hide();
       console.log($(this).closest('.card-body').find('[data-lang="'+lang+'"]'));
       $(this).closest('.card-body').find('[data-lang="'+lang+'"]').show();
       $(this).siblings().removeClass('active');
       $(this).addClass('active')
    })

});

function auto_grow(element) {
      element.style.height = "5px";
      if (element.scrollHeight > 80) {
          element.style.height = 5 + "px"; element.style.height = (element.scrollHeight + 10) + "px";
      }
}

function set_survey_order(item) {
    var index = item.index();
    var questionID = item.attr('id').split('-')[1];
   $.ajax({
       url: "/set_survey_order/",
       type: "POST",
       dataType: 'json',
       data: {index: index, id: questionID},
       success: function (result) {

       }
   })
}

