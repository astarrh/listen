from django.forms import ModelForm

from .models import ContactSubscription, District


class ContactSubscriptionForm(ModelForm):

    class Meta:
        model = ContactSubscription
        fields = ['name', 'email']


class DistrictForm(ModelForm):
    class Meta:
        model = District
        fields = ['name', 'organization']
