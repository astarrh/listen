from django.db import models

from .choices import CONTENT_OPTIONS, SECTION_OPTIONS


class District(models.Model):
    name = models.CharField(max_length=36)
    organization = models.ForeignKey("Home.Organization", on_delete=models.CASCADE)


class Project(models.Model):
    name = models.CharField(max_length=48)
    description = models.TextField(blank=True, null=True)
    headline = models.CharField(null=True, blank=True, max_length=48)
    slug = models.SlugField()
    organization = models.ForeignKey("Home.Organization", on_delete=models.CASCADE)
    centered_page_text = models.BooleanField(default=False)
    district = models.ForeignKey(District, on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        
        return self.name

    def latest_event(self):
        return self.event_set.get_latest


class Event(models.Model):
    project = models.ForeignKey(Project, on_delete=models.PROTECT, related_name="events")
    name = models.CharField(max_length=48)
    start = models.DateTimeField()
    end = models.DateTimeField(blank=True, null=True)
    topic = models.CharField(max_length=48, blank=True, null=True)
    instructions = models.CharField(max_length=512, blank=True, null=True)
    registration = models.BooleanField(default=False)
    registration_url = models.URLField(blank=True, null=True)
    registration_details = models.CharField(max_length=48, blank=True, null=True)

    class Meta:
        ordering = ['start']


class Registrant(models.Model):
    event = models.ForeignKey(Event, models.CASCADE)
    name = models.CharField(max_length=128)
    phone = models.CharField(blank=True, null=True, max_length=24)
    email = models.EmailField(blank=True, null=True, max_length=64)
    details = models.CharField(blank=True, null=True, max_length=64)


class ProjectPageSection(models.Model):
    project = models.ForeignKey(Project, related_name="sections", on_delete=models.CASCADE)
    title = models.OneToOneField("Home.Copy", null=True, on_delete=models.SET_NULL)
    configuration = models.CharField(max_length=24, choices=SECTION_OPTIONS)
    event = models.ForeignKey(Event, null=True, on_delete=models.SET_NULL)
    order = models.IntegerField()

    class Meta:
        ordering = ['order']


class ProjectPageContent(models.Model):
    copy = models.ForeignKey("Home.Copy", on_delete=models.PROTECT, blank=True)
    section = models.ForeignKey(ProjectPageSection, on_delete=models.CASCADE)
    display_html = models.CharField(choices=CONTENT_OPTIONS, max_length=512)
    order = models.IntegerField()

    class Meta:
        ordering = ['order']


class ContactSubscription(models.Model):
    name = models.CharField(max_length=256)
    email = models.EmailField()
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    user = models.ForeignKey("Home.ListenUser", blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.name
