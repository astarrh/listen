from django.template.defaultfilters import slugify
from django.urls import reverse_lazy
from django.http import JsonResponse
from json import loads as unpack

from .models import Project, ContactSubscription, ProjectPageSection, ProjectPageContent, District
from .forms import ContactSubscriptionForm, DistrictForm
from .choices import CONTENT_OPTIONS_DICT
from Home.models import Copy, TranslatedText, Language, ListenUser
from tools import request_processors as rp


def get_districts(request):
    allowed, response = rp.check_if_method_is_post(request)
    if allowed:
        districts = District.objects.filter(organization=request.user.organization)
        response = rp.set_successful_response_for_indexed_queryset(districts, fields=['name'])
    return response


def manage_district(request):
    allowed, response = rp.check_if_method_is_post(request)
    if allowed:
        request_data = unpack(request.body)
        action = request_data.pop('action')
        pk = request_data.pop('pk')
        if action == 'delete':
            District.objects.get(id=pk).delete()
        else:
            form = None
            request_data['organization'] = request.user.organization
            if action == 'create':
                form = DistrictForm(request_data)
            elif action == 'update':
                form = DistrictForm(request_data, instance=District.objects.get(id=pk))
            if not form:
                response = rp.set_rejected_response()
            elif form.is_valid():
                obj = form.save()
                response = rp.set_successful_response_for_object(obj)
            else:
                response = rp.set_response_with_form_errors(form)
        return response


def add_project(request):
    response = {'success': False}
    if request.method != "POST":
        return JsonResponse(response)
    name = request.POST.get('name')
    slug = slugify(name)

    _, created = Project.objects.get_or_create(name=name, slug=slug, organization=request.user.organization)
    if created:
        link = reverse_lazy("project", kwargs={'slug': slug})
        string = '<a style="font-size: 16px; margin-bottom: 3px" href="{}">{}</a><br>'.format(link, name)
        response = {'success': True, 'string': string}
    else:
        response = {'success': False, 'string': "A project with this name already exists."}
    return JsonResponse(response)


def arrange_sections(request):
    response = {'success': False}
    if request.method != "POST":
        return JsonResponse(response)
    slug = request.POST.get('slug')
    position_a = int(request.POST.get('positionA'))
    position_b = request.POST.get('positionB')
    project = Project.objects.get(organization=request.user.organization, slug=slug)
    if position_b:
        position_b = int(position_b)
        object_a = ProjectPageSection.objects.filter(project=project, order=position_a).first()
        object_b = ProjectPageSection.objects.filter(project=project, order=position_b).first()
        object_a.order = position_b
        object_a.save()
        if object_b:
            object_b.order = position_a
            object_b.save()
    else:
        ProjectPageSection.objects.filter(project=project, order=position_a).delete()

    response = {'success': True}
    return JsonResponse(response)


def create_subscription(request, slug):
    project = Project.objects.get(slug=slug)

    response = {'success': False}
    if request.method != "POST":
        return JsonResponse(response)

    email = request.POST.get('email')

    existing_user = None
    if ListenUser.objects.filter(email=email).exists():
        existing_user = ListenUser.objects.filter(email=email).last()

    if ContactSubscription.objects.filter(project=project, email=email).exists():
        response = {'success': True, 'existing_email': True}
        return JsonResponse(response)

    form = ContactSubscriptionForm(request.POST)
    if form.is_valid():
        obj = form.save(commit=False)
        obj.project = project
        if existing_user:
            obj.user = existing_user
        obj.save()
        response = {"success": True, "existing_user": True if existing_user else False, "existing_email": False}
    else:
        response['errors'] = form.errors.as_data()

    return JsonResponse(response)
