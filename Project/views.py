from django.views.generic import DetailView, TemplateView, CreateView, UpdateView
from .models import Project

from Home.models import Organization
from Survey.models import WeeklyQuestion


class ManageView(TemplateView):
    template = ""

    def projects(self, **kwargs):
        data = {}
        for project in Project.objects.all():
            data[project.id] = project.name


class CreateProjectView(CreateView):
    model = Project
    fields = ['name', 'description']

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.organization = Organization.objects.get()
        self.object.save()
        return super(ModelFormMixin, self).form_valid(form)


class ProjectView(DetailView):
    model = Project
    template_name = "project.html"
    user_can_edit = True

    def question(self):
        return WeeklyQuestion.questions.current()


class SampleProjectView(TemplateView):
    template_name = 'project.html'


