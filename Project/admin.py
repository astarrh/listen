from django.contrib import admin

from .models import Project, ContactSubscription, ProjectPageSection, ProjectPageContent, District

admin.site.register(Project)
admin.site.register(District)
admin.site.register(ContactSubscription)
admin.site.register(ProjectPageSection)
admin.site.register(ProjectPageContent)
