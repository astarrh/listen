TITLE = '<h3 class="h3 mb-3"><strong>{}</strong></h3>'
PARAGRAPH = '<p>{}</p>'
BULLET = '<ul style="padding-top: 0; padding-bottom:0"><li class="bullet-point">{}</li></ul>'
IMAGE = '<img src="{}" width="100%">'
LINK = '<a class="content_link" href={}>{}</a>'

CONTENT_OPTIONS = (
    (TITLE, 'title'),
    (PARAGRAPH, 'paragraph'),
    (BULLET, 'bullet'),
    (IMAGE, 'image'),
    (LINK, 'link')
)

CONTENT_OPTIONS_DICT = {
   'title': TITLE, 'paragraph': PARAGRAPH, 'bullet': BULLET, 'image': IMAGE, 'link': LINK, "embedded_code": ""
}

ANNOUNCEMENT = "announcement"  # Centered Text
INFORMATION = "information"   # Left-aligned Text
EVENT = "event"  # Pre-formatted Event Instructions

SECTION_OPTIONS = (
    (ANNOUNCEMENT, "announcement"),
    (INFORMATION, "information"),
    (EVENT, "event"),
)
