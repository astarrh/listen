from django.urls import path

from Project import actions

urlpatterns = [
    path('add_project/', actions.add_project, name='add_project'),
    path('get_districts', actions.get_districts, name="get_districts"),
    path('manage_district/', actions.manage_district, name='manage_district')
]


