from http import HTTPStatus
from itertools import chain

from django.http import JsonResponse


def full_model_to_dict(instance):
    opts = instance._meta
    data = {}
    for f in chain(opts.concrete_fields, opts.private_fields):
        data[f.name] = f.value_from_object(instance)
    for f in opts.many_to_many:
        data[f.name] = [i.id for i in f.value_from_object(instance)]
    return data


def check_if_method_is_post(request, data=None):
    if request.method != "POST":
        return False, JsonResponse({
                    'message': "Method not allowed",
                    'object': None
                }, status=HTTPStatus.METHOD_NOT_ALLOWED
        )
    else:
        return True, JsonResponse({
                    'message': "Operation Successful.",
                    'object': data
                }, status=HTTPStatus.ACCEPTED
        )


def output_if_method_is_post(request, output, status):
    if request.method != "POST":
        return JsonResponse({
                    'status': 'false',
                    'message': "Method not allowed"
                }, status=HTTPStatus.METHOD_NOT_ALLOWED
        )
    else:
        return JsonResponse(output, status=status)


def set_response_with_form_errors(form):
    status = HTTPStatus.NO_CONTENT
    message = form.errors.as_json()
    print(message)
    data = None
    return JsonResponse({'message': message, 'object': data}, status=status)


def set_successful_response(message="Operation Successful.", data=None, **kwargs):
    return JsonResponse({'message': message, 'object': data}, status=HTTPStatus.ACCEPTED)


def set_rejected_response(message="Operation Rejected", data=None, **kwargs):
    return JsonResponse({'message': message, 'object': data}, status=HTTPStatus.UNPROCESSABLE_ENTITY)


def set_successful_response_for_object(obj, message="Operation Successful.", fields=None, **kwargs):
    status = HTTPStatus.ACCEPTED
    message = message
    if fields:
        data = {}
        for field in fields:
            data[field] = getattr(obj, field)
    else:
        data = full_model_to_dict(obj)
    if kwargs:
        data.update(**kwargs)
    return JsonResponse({'message': message, 'object': data}, status=status)


def set_successful_response_for_queryset(obj, message="Operation Successful."):
    status = HTTPStatus.ACCEPTED
    message = message
    data = list(obj.values())
    return JsonResponse({'message': message, 'object': data}, status=status)


def set_successful_response_for_indexed_queryset(qs, message="Operation Successful.", fields=None):
    status = HTTPStatus.ACCEPTED
    message = message
    data = {}
    for obj in qs:
        if fields:
            obj_data = {}
            for field in fields:
                obj_data[field] = getattr(obj, field)
            data[obj.id] = obj_data
        else:
            data[obj.id] = full_model_to_dict(obj)
    print(data)
    return JsonResponse({'message': message, 'object': data}, status=status)