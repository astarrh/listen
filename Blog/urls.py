from django.urls import path

from Blog import views

urlpatterns = [
    path('', views.RedirectToLatestPost.as_view(), name='latest_post'),
    path('create_post/', views.CreatePostView.as_view(), name='create_post'),
    path('drafts/', views.DraftsListView.as_view(), name='drafts'),
    path('edit_post/<slug:slug>', views.UpdatePostView.as_view(), name='edit_post'),
    path('index/', views.IndexView.as_view(), name='index'),
    path('<slug:slug>/', views.PostView.as_view(), name='view_post'),
]
