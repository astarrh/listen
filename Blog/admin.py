from django.contrib import admin
from Blog.models import Blog, Post

admin.site.register(Blog)
admin.site.register(Post)
