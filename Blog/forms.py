from django import forms

from .models import Post


class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ['title', 'author', 'project', 'body']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['title'].widget.attrs.update({'class': 'border-b w-1/3'})
        self.fields['author'].widget.attrs.update({'class': 'border-b w-1/3'})
        self.fields['project'].widget.attrs.update({'class': 'border-b w-1/3'})
