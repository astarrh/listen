from datetime import datetime

from django.db import models
from django.urls import reverse_lazy

from django.utils.text import slugify
from ckeditor.fields import RichTextField

STATUS = (
    (0, "Draft"),
    (1, "Publish")
)


class Blog(models.Model):
    organization = models.ForeignKey("Home.Organization", on_delete=models.CASCADE)

    def __str__(self):
        return "Blog for " + self.organization.name


class Post(models.Model):
    title = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    body = RichTextField(blank=True, null=True)
    author = models.CharField(max_length=256)
    status = models.IntegerField(choices=STATUS, default=0)
    published_on = models.DateField(null=True, blank=True)
    project = models.ForeignKey('Project.Project', on_delete=models.SET_NULL, null=True, blank=True)
    blog = models.ForeignKey(Blog, on_delete=models.PROTECT)

    class Meta:
        ordering = ['published_on']

    def get_absolute_url(self):
        return reverse_lazy("Blog:view_post", args=[self.slug])

    def save(self, *args, **kwargs):
        # TODO: This needs to be set to properly find the correct Blog
        self.blog = Blog.objects.all().first()
        self.slug = slugify(self.title)
        super(Post, self).save(*args, **kwargs)

    def publish(self):
        self.status = 1
        self.published_on = datetime.now().date()
        self.save()

    @property
    def is_published(self):
        return True if self.status == 1 else False
