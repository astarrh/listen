from django.views.generic import CreateView, DetailView, RedirectView, UpdateView, ListView, TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

from Home.views import LanguageMixin
from Blog.forms import PostForm
from Blog.models import Post


class CreatePostView(LanguageMixin, LoginRequiredMixin, UserPassesTestMixin, CreateView):
    model = Post
    form_class = PostForm
    template_name = "create_post.html"

    def test_func(self):
        return self.request.user.is_staff


class UpdatePostView(LanguageMixin, LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    form_class = PostForm
    template_name = "create_post.html"

    def test_func(self):
        return self.request.user.is_staff


class DraftsListView(LoginRequiredMixin, UserPassesTestMixin, ListView):
    template_name = 'drafts.html'
    model = Post

    def test_func(self):
        return self.request.user.is_staff


class PostView(DetailView):
    template_name = "view_post.html"
    model = Post

    @property
    def is_preview(self):
        return True if self.get_object().status == 0 else False

    def is_editable(self):
        return self.request.user.is_staff if not self.request.user.is_anonymous else False

    def post(self, request, *args, **kwargs):
        publish_requested = True if request.POST.get('publish') == "true" else False
        if publish_requested and self.is_preview and self.request.user.is_staff:
            self.get_object().publish()
            return self.get(self, request, *args, **kwargs)
        else:
            return self.get(self, request, *args, **kwargs)


class RedirectToLatestPost(RedirectView):
    pattern_name = 'Blog:view_post'

    def get_redirect_url(self, *args, **kwargs):
        args = [Post.objects.filter(status__gt=0).latest('published_on').slug]
        return super().get_redirect_url(*args, **kwargs)


class IndexView(TemplateView):
    template_name = 'directory.html'

    def get_posts(self):
        return Post.objects.filter(blog__organization=1).order_by('-published_on')
        
    def posts_by_year(self):
        output = {}
        for post in self.get_posts():
            if post.published_on:
                year = post.published_on.year
                data = {'slug': post.slug, 'title': post.title}
                if year in output:
                    output[year].append(data)
                else:
                    output[year] = [data]
        return output

    def posts_by_project(self):
        output = {}
        for post in self.get_posts():
            project = post.project.name if post.project else "Various"
            data = {'slug': post.slug, 'title': post.title}
            if project in output:
                output[project].append(data)
            else:
                output[project] = [data]
        return output
