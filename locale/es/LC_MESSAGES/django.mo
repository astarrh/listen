��    X      �     �      �  �   �  �   1  �   �  �   �	  O   �
  �   �
  ~   r  �   �  o   �  s   5  �  �    +  D  4  v   y  L   �  R   =  I   �  I   �  W   $  2  |  �  �  l   `  l   �  �   :     �  +   �  �   �  7   �  	   �     �  )   �               "     1     C     J  '   Y  /   �     �     �     �     �  	   �  =   �  9     Y  Y  �   �     Y   b   l      �   w   �!     h"     o"     �"     �"     �"     �"     �"     �"  6   �"     #     #     #     &#     6#     G#     U#     ^#     m#  �   u#     $     #$     >$     R$     c$  +   $  �   �$  (   5%     ^%  �   k%  �   �%  )   �&  C   �&      �&     '     �'     �'  [   �(  �   !)  `   �)  �   *  2   �*  �   1+  W   �+  �   ,  =   �,  U   ,-  d  �-  �  �.  >  {0  Z   �2  (   3  '   >3  #   f3     �3  1   �3  B  �3  T  6  _   t8  Z   �8  �   /9     �9  0   �9  �   :  @   �:      ;     1;  P   C;     �;  	   �;     �;     �;     �;  8   �;  -   <  8   A<     z<     �<     �<     �<  
   �<  V   �<  5   =  �   K=  �   F>     �>  u   ?  z  z?  �   �@     �A     �A     �A  	   �A     �A     �A     �A  8   �A  M   "B  	   pB  	   zB     �B     �B     �B     �B     �B     �B     �B  �   �B     �C     �C     �C     �C     D  2   D  �   RD  &   �D     E  �   5E  �   �E  9   |F  <   �F  "   �F  �   G     �G     
       Q       ;   (   @   I          =             O   E   2      1   -      M   F   J              +   <   9   A       *   X   W                6   :            P      ?      >              %       0   K       '               /          8              7   ,   C   !   D           #   )       4       H                     B   T   .       V      &      	           3   G   "   S                                     R                   5   L       U   $   N                   
                            Already have an account? Please <a class="text-blue-400" href="/accounts/login">sign in here.</a>
                         
                            Developing our community studies and processing responses in order to provide rich and informative data to decision-makers in the community.
                         
                            Don't have an account? Please <a class="text-blue-400" href="/accounts/signup">create one here.</a>
                         
                            Engaging more people who live and work in Shafter. This includes survey translation, accessibility accommodations as well as materials and small events to raise awareness and promote participation.
                         
                            Enter Survey Access Code:
                         
                            Listen To Shafter will advise community leaders and structure activities based on your responses.
                         
                            Our own administrative costs associated with managing Listen To Shafter.
                         
                            Providing administrative support to new or existing local community benefit efforts and to those interested in working to improve quality of life in Shafter.
                         
                            This is your chance to share what you think about Shafter
                         
                        Let's work together to figure out the best plan for Shafter's future.
                     
                    If we reach our goal of 250 responses, we will put together a report which will be available here on
                    our website for anyone to download and review. We will also deliver a copy of the report to city
                    council and staff, local civic clubs, and other concerned parties who are doing relevant work in Shafter.
                     
                    In order to protect your privacy, your individual written answers will not be made publicly
                    available. Instead, our research team will identify themes within your answers which will then be
                    used in an aggregate analysis of the data to be made available for public use. If raw data is ever
                    requested or delivered, it will only include these identified themes and not your written answers
                    themselves.
                     
                    There are many decisions being made every day within the halls of Shafter's government, civic clubs
                    and among third party donors and contributors about how to allocate resources within the community. We
                    are trying to build a data profile that will help everyone obtain a better understanding of the people
                    who live and work in Shafter. This is all being undertaken in pursuit of more impactful and more
                    equitable outcomes in Shafter's growth and development.
                     
                    We invite anyone who lives or works in Shafter to participate in this study.
                     
                    What is the purpose of this study?
                     
                    What will be done with this information?
                     
                    Who is eligible to participate?
                     
                    Who will be reading my answers?
                     
                    Will my answers be published on your website?
                     
                    Your answers will be reviewed by our designated research team. However, your answers will not be
                    connected to any other information that could be used to identify you and they will not be read in
                    succession with your other answers. All responses will be kept anonymous. Contact information that
                    you choose to share with us will be used only by our computer system to invite you to participate in
                    further studies that may be relevant to you.
                     
                Founded in 2018, Listen To Shafter is a comprehensive community benefit organization dedicated to improving
                the quality of life and resiliency of our community. We pursue a holistic approach that promotes literacy
                and education among children and youth, encourages cooperation and public participation among teens & adults,
                and incubates and fosters growing organizations to affect positive, empirical transformation in our community.
                We continuously strive to operate programs that are conceived, designed and evaluated through a process of
                conversation with our neighbors.
                 
                View your dashboard to participate in our community-wide research project!
                 
            If you have a survey code, please enter it here to participate or see the results:
             A crucial component of citizenship is taking an honest look at existing pain points among our neighbors and responding to them appropriately. About Amplify and respond to marginalized voices. Any work being done in the community can always benefit from better information! Our methodology is designed to arm local leaders with crucial data. As promised, your responses will remain 100% anonymous. Be Heard! Begin Survey Catalogue and celebrate community assets. Completed Surveys Connect Create Account Create My Account Donate Email or Phone Engage and strengthen local businesses. Enhance the capacity of public & civic sectors. Enter Finish Forgot Password? Go Back Help Out! Identify and respond to community hardships and service gaps. If you do not recognize this information as yours, please If you haven't yet, please be sure to complete the other surveys on your dashboard. Your participation is incredibly helpful to everyone who is working to make our city the best place it can be! Also, please consider sharing this website with some fellow Shafter people and inviting them to participate as well. We need every last person's help! If you'd like our system to remember your answers so you won't lose your progress if you get interrupted, please sign up or log in to your Listen To Shafter profile: Keep me signed in: Listen To Shafter is a registered 501(c)(3) organization and all contributions are tax deductible. Listen To Shafter is dedicated to learning about the people who live and work here. If you have the means, please consider sponsoring us with a small monthly donation. Your assistance will help us build a more complete picture of our community by giving a voice to more of your neighbors! Local businesses are an important part of a healthy community. We are excited to hear what ours in Shafter have to say! Log In Log In to Dashboard Log Out My Dashboard New Surveys News Password Phone Number or Email Address Please alert me of available surveys via SMS or Email: Proceed Projects Read Our Blog Repeat Password Repeat Password: Save Settings Settings Shafter Survey Sign Up Sometimes an introduction is all it takes. If you have a special passion or talent that you'd like to contribute to the benefit of our town, please come meet with us! Submit Support Listen To Shafter! Surveys In Progress Take the survey! Thank You For Participating Thanks again for taking part in this study! The hardest voices to hear are sometimes the most informative. A special effort is required to make sure everyone gets a chance to speak. Today is %(month)s %(day)s. %(question)s Unsubscribe: We love monthly commitments, but will gladly accept one-time donations as well! You can use the button  above to get started, or mail a check to: What's so great about Shafter? We want to make sure we understand the answer to this question so that none of our strengths are under-utilized. Why participate in our community surveys? You are currently logged in with the following account information: Your contributions help us with: Your responses are 100% anonymous. We are an independent non-profit and are not affiliated with the City of Shafter in any way. log out Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-09-10 14:23-0700
Last-Translator: 
Language-Team: 
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0
 
 ¿Ya tienes una cuenta? <a class="text-blue-400" href="/accounts/login">Entre aquí.</a>  
 Desarrollar nuestros estudios comunitarios y procesar respuestas para proveer datos útiles  e informativos a quienes toman decisiones en la comunidad  
 ¿No tienes una cuenta?  <a class="text-blue-400" href="/accounts/signup">Cree uno aquí.</a>  
 Involucrar a más personas que viven y trabajan en Shafter. Esto incluye traducción de encuestas, adaptaciones de accesibilidad, así como materiales y pequeños eventos para crear conciencia y promover la participación.  
 Introduzca el código de acceso de la encuesta:  
 Listen to Shafter compartirá consejos con los lideres comunitarios y estructurará las actividades en función de las respuestas de la comunidad.  
 Nuestros costos administrativos asociados con el mantenimiento de Listen to Shafter.  
Proveer apoyo administrativo a los proyectos de beneficio de la comunidad local nuevos o existentes y a aquellos interesados en trabajar para mejorar la calidad de vida en Shafter.
                         
 Esta es tu oportunidad de compartir que opinas de Shafter.  
Trabajemos juntos para descubrir cuales es el mejor plan para el futuro de Shafter.  
Si alcanzamos nuestro objetivo de 250 respuestas, haremos un reporte que sea disponible aquí en nuestro sitio de web para cualquier persona que lo quiere ver o bajar. Tambien, entregaremos una copia al consejo de la ciudad y los empleados de la ciudad, los clubes cívicos y otras partes interesadas que están realizando un trabajo relevante en Shafter. 
Para proteger su privacidad, sus respuestas individuales por escrito no se harán públicas. En cambio, nuestro equipo de investigación identificará temas dentro de sus respuestas que luego serán utilizado en un análisis agregado de todos los datos para uso público. Si los datos brutos alguna vez son solicitados o entregados, solo incluirá estos temas identificados y no sus respuestas escritas 
 Hay muchas decisiones que se toman todos los días dentro de los pasillos del gobierno de Shafter, clubes cívicos
                    y entre donantes y contribuyentes externos sobre cómo asignar recursos dentro de la comunidad. Nosotros
                    están tratando de crear un perfil de datos que ayude a todos a comprender mejor a las personas
                    que viven y trabajan en Shafter. Todo esto se está llevando en la búsqueda de fines más impactantes y 
                    resultados más equitativos en el crecimiento y desarrollo de Shafter. 
Invitamos a cualquier persona que viva o trabaje en Shafter a participar en este estudio. 
 Cual es el propósito de este estudio? 
¿Qué se hará con esta información? 
¿Quién se puede para participar? 
 Quien va a leer mi respuesta? 
¿Se publicarán mis respuestas en su sitio web? 
 Sus respuestas serán revisadas por nuestro equipo de investigación. Sin embargo, sus respuestas no serán
                    conectado a cualquier otra información que se pueda usar para identificarlo y no se leerá en
                    sucesión con sus otras respuestas. Todas las respuestas se mantendrán anónimas. Información de contacto que
                    que elijas compartir con nosotros será utilizado únicamente por nuestro sistema informático para invitarlo a participar en
                    estudios adicionales que puedan ser relevantes para ti. 
 Fundada en el 2018, Listen to Shafter, es una organización de beneficio comunitario, dedicada a mejorar la calidad de vida y fortaleza de nuestra comunidad. Trabajamos de manera holístico para promover la alfabetización y la educación de niños y jóvenes, fomentar la cooperación y la participación pública entre adolescentes y adultos, y estimular el crecimiento en organizaciones nacientes para lograr una transformación medibles en la comunidad. Nos esforzamos proveer programas que son formados, diseñados , evaluados a través de un proceso de conversación con nuestros vecinos  
¡Vea su tablero para participar en nuestro proyecto de investigación para toda la comunidad! 
Si tienes un código de encuesta, ingréselo aquí para participar o ver los resultados:  Un componente fundamental de la ciudadanía es observar con sinceridad los puntos débiles existentes entre nuestros vecinos y responder a ellos apropiadamente. Acerca de Nosotros Amplificar y responder a las voces marginadas.   ¡Cualquier trabajo que se realice en la comunidad siempre se puede beneficiar de una mejor información! Nuestra metodología está diseñada para proporcionar datos importantes a los líderes locales. Como fue prometido, sus respuestas permanecerán 100% anónimas. ¡Sé escuchado! Comenzar encuesta Compartir y celebrar factor o recurso que mejore la capacidad de la comunidad.   Encuestas completadas Comunicar Crear una cuenta Crea mi cuenta Donar Dirección de correo electrónico o número de teléfono Involucrar y fortalecer los negocios locales. Mejorar la capacidad de los sectores público y cívico. Entrar Terminar ¿Se te olvidó tu contraseña? Regresar Ayúdanos! Identificar y responder a las dificultades de la comunidad y las faltas de servicio.   Si no reconoce esta información como suya, por favor Si aún no lo ha hecho, por favor, asegúrese de completar las demás encuestas en su panel de información. ¡Su participación es sumamente útil para todos los que están trabajando para hacer de nuestra ciudad el mejor lugar donde se puede estar! Si desea que nuestro sistema recuerde sus respuestas para que no pierda su progreso si lo interrumpen, regístrese o inicie sesión en su perfil de Listen To Shafter: Mantenerme contectado: Listen To Shafter es una organización registrada 501 (c) (3) y todas las contribuciones son deducibles de impuestos. Listen To Shafter es una organización para el beneficio de la comunidad que se dedica a aprender sobre las personas que viven y trabajan en nuestra comunidad. Si usted tiene los medios, por favor, considere patrocinarnos con una pequeña donación mensual. ¡Con su ayuda podremos construir una imagen más completa de nuestra comunidad para que más vecinos se hagan escuchar! Las empresas locales son una parte importante de una comunidad saludable. ¡Nos complace escuchar lo que la comunidad de Shafter tiene que decir! Iniciar Sesión Entrar a mi portal Cerrar sesión Mi Portal Nuevas Encuestas Noticias Contraseña Número de teléfono o dirección de correo electrónico Avíseme de las encuestas disponibles a través de SMS o correo electrónico: Continuar Proyectos Lee Nuestro Blog Repite la contraseña Repite la Contraseña: Guardar Ajustes Ajustes Encuesta de Shafter Inscribirse A veces presentarse es todo lo que se necesita. Si usted tiene una pasión o un talento especial con el que le gustaría contribuir al beneficio de nuestra ciudad, por favor, ¡visítenos! Enviar Apoya a Listen to Shafter! Encuestas En proceso ¡Complete esta encuesta! Gracias por participar ¡Gracias de nuevo por participar en este estudio! Las voces más difíciles de escuchar son a veces las más informativas. Se requiere un esfuerzo especial para asegurar que todos tengan la oportunidad de hablar. Hoy es %(month)s %(day)s. %(question)s Cancelar la suscripción Nos encantan los compromisos mensuales, pero con gusto también aceptaremos donaciones únicas! Puede usar el botón de arriba para comenzar o enviar un cheque por correo a: ¿Qué tiene de bueno Shafter? Queremos asegurarnos de que entendemos la respuesta a esta pregunta para no infrautilizar ninguna de nuestras fortalezas. Porque debes participar en las encuestas de la comunidad? Ha iniciado sesión con la siguiente información de cuenta: Sus contribuciones nos ayudan con: Sus respuestas son 100% anónimas. Somos una organización independiente sin fines de lucro y no estamos afiliados con la ciudad de Shafter de ningún modo. cerrar sesión 