def extract_ti_tag(data):
    if not data or isinstance(data[0], str):
        output = data
    else:
        output = [d.get('text') for d in data]
    return output
