from Survey.models import Code, Question, TextAnswer
from Survey.choices import MULTI
from Survey.utils.tags import extract_ti_tag


def save_response(question, answer, moment, prev_answer=None, **kwargs):
    if prev_answer and moment < prev_answer.moment:
        return False, "Answer is out of date."
    form = question.get_answer_model_class(question=question).get_form()({'response': answer}, instance=prev_answer)
    if form.is_valid():
        answer = form.save(commit=False)
        answer.moment = moment
        if not prev_answer:
            answer.respondent = kwargs.get('respondent', None)
            answer.session_key = kwargs.get('session_key', None)
            answer.question = question
        answer.save()
        print(answer.id)
        return True, "Success"
    else:
        return False, form.errors.as_json()


def process_response(question, response, moment, **kwargs):
    success, message = False, "Unknown Error"
    respondent = kwargs.get('respondent', None)
    session_key = kwargs.get('session_key', None)
    if question.response_format == MULTI:
        prev_answers = question.choiceanswer_set.filter(
            question=question, respondent=respondent, session_key=session_key
        )
        prev_answers.delete()
        for answer in response:
            success, message = save_response(question, answer, moment, **kwargs)
            if not success:
                break
    else:
        prev_answer = question.get_answer_model_class.objects.filter(
            question=question, respondent=respondent, session_key=session_key
        ).first()
        success, message = save_response(question, response, moment, prev_answer, **kwargs)
    return success, message


def process_response_code(question, codes, moment, session_key):
    codes = extract_ti_tag(codes)
    for code in codes:
        code, _ = Code.objects.get_or_create(text=code)
        TextAnswer.objects.create(question=question, session_key=session_key, moment=moment, code=code)
    return True, "Success"
