from json import dumps as to_json
import datetime

from django.db import models
from django.contrib.postgres.functions import RandomUUID
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import ArrayField

from Survey import choices
from Survey.html import format_results
from Survey.utils.slug import unique_slugify
from .managers import WeeklyQuestionManager, QuestionnaireManager, AnswerManager


class Population(models.Model):
    label = models.CharField(max_length=48)
    description = models.TextField()


class Respondent(models.Model):
    user = models.OneToOneField('Home.ListenUser', on_delete=models.CASCADE)
    organization = models.ForeignKey('Home.Organization', on_delete=models.PROTECT, null=True)
    date_of_birth = models.DateField(null=True, blank=True)
    gender = models.IntegerField(choices=choices.GENDERS, null=True, blank=True)
    heritage = models.CharField(blank=True, null=True, max_length=48)
    eligible_for = models.ManyToManyField('Questionnaire', related_name='eligible_users', blank=True)
    started = models.ManyToManyField('Questionnaire', related_name='started', blank=True)
    finished = models.ManyToManyField('Questionnaire', related_name='finished', blank=True)
    district = models.ForeignKey('Project.District', blank=True, null=True, on_delete=models.SET_NULL)
    project = models.ForeignKey('Project.Project', blank=True, null=True, on_delete=models.SET_NULL)
    updated = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.user.username


class Questionnaire(models.Model):
    label = models.CharField(max_length=124)
    organization = models.ForeignKey('Home.Organization', on_delete=models.PROTECT, null=True)
    date_opened = models.DateField(null=True, blank=True)
    date_closed = models.DateField(null=True, blank=True)
    is_qualitative = models.BooleanField(default=False)
    is_open_to_all = models.BooleanField(default=False)
    introduction = models.TextField(blank=True)
    slug = models.CharField(max_length=24, blank=True, null=True)
    survey_code = models.CharField(max_length=12, blank=True, null=True)
    hidden = models.BooleanField(default=False)
    login_is_required = models.BooleanField(default=False)
    population = models.ForeignKey(Population, blank=True, null=True, on_delete=models.SET_NULL)
    district = models.ForeignKey('Project.District', blank=True, null=True, on_delete=models.SET_NULL)
    project = models.ForeignKey('Project.Project', blank=True, null=True, on_delete=models.SET_NULL)

    available = QuestionnaireManager()
    objects = models.Manager()

    def __str__(self):
        return self.label

    def save(self, **kwargs):
        slug_str = self.label
        unique_slugify(self, slug_str)
        super(Questionnaire, self).save(**kwargs)

    def ordered_questions(self):
        return self.question_set.all().order_by('position')

    def count_responses(self):
        return self.started.all().count()

    def has_eligible_responses(self):
        response_exists = TextAnswer.objects.filter(question__questionnaire=self, resolution__isnull=True).exists()
        return True if response_exists else False

    def is_closed(self):
        today = datetime.date.today()
        return True if today <= self.date_opened or today >= self.date_closed else False

    def average_response_rate(self):
        answer_counts = [question.get_response_count() for question in self.question_set.all()]
        average_response_per_question = round(sum(answer_counts) / len(answer_counts), 2) if len(answer_counts) > 0 else 0
        return average_response_per_question

    def builder_data(self):
        data = {
            'label': self.label,
            'survey_code': self.survey_code,
            'population': self.population.id if self.population else "",
            'district': self.district.id if self.district else "",
            'project': self.project.id if self.project else "",
            'login_is_required': self.login_is_required,
            'introduction': self.introduction,
            'date_range': {
                'start': self.date_opened.isoformat(),
                'end': self.date_closed.isoformat(),
            },
        }
        translations = self.translation_set.filter(language__code='es')
        if translations.exists():
            data['intro_translation'] = translations.first().text
        return data

    def duplicate(self):
        kwargs = {}
        for field in self._meta.fields:
            kwargs[field.name] = getattr(self, field.name)
        kwargs.pop('id')
        new_instance = self.__class__(**kwargs)

        new_instance.label += " Copy"
        new_instance.closed = False
        new_instance.date_opened = None
        new_instance.date_closed = None

        new_instance.save()

        questions = self.question_set.all()
        new_questions = []
        for question in questions:
            question_kwargs = {}
            for field in question._meta.fields:
                question_kwargs[field.name] = getattr(question, field.name)
            question_kwargs.pop('id')
            question_kwargs['questionnaire'] = new_instance
            new_questions.append(questions.model(**question_kwargs))
        questions.model.objects.bulk_create(new_questions)

        return new_instance


class Question(models.Model):
    questionnaire = models.ForeignKey(Questionnaire, on_delete=models.CASCADE)
    response_format = models.CharField(choices=choices.RESPONSE_FORMATS, default=choices.LONG_TEXT, max_length=24)
    text = models.TextField(blank=True, null=True)
    tags = ArrayField(models.CharField(max_length=1024), blank=True, null=True)
    options = ArrayField(models.CharField(max_length=512), blank=True, null=True)
    position = models.IntegerField(blank=True, null=True)

    class Meta:
        ordering = ['position']

    def __str__(self):
        return "{} #{}".format(self.questionnaire.label, self.position)

    @property
    def get_answer_model_class(self):
        ct = ContentType.objects.get(app_label="Survey", model=choices.MODEL_STRINGS[self.response_format])
        return ct.model_class()

    def builder_data(self):
        translation_set = self.translation_set.filter(language__code='es')
        output = {
            'pk': self.id,
            'text': self.text,
            'response_format': self.response_format,
            'translation': translation_set.first().text if translation_set.exists() and translation_set.first().text else "",
            'tags': self.tags,
            'options': self.options,
            'translatedOptions': translation_set.first().options if translation_set.exists() and translation_set.first().options else []
        }
        return output

    def get_response(self, answer_objects):
        if answer_objects.exists():
            if self.response_format == choices.MULTI:
                answer_data = list(answer_objects.values_list('response', flat=True))
            else:
                answer_data = answer_objects.last().response
        else:
            if self.response_format == choices.MULTI:
                answer_data = []
            elif self.response_format == choices.RANGE:
                answer_data = ""
            else:
                answer_data = None
        return answer_data

    def get_user_response(self, user):
        respondent = Respondent.objects.get(user=user)
        answer_objects = self.get_answer_model_class.objects.filter(question=self.id, respondent=respondent)
        return self.get_response(answer_objects)

    def get_anonymous_response(self, key):
        answer_objects = self.get_answer_model_class.objects.filter(question=self.id, session_key=key)
        return self.get_response(answer_objects)

    def get_all_responses(self):
        return self.get_answer_model_class.objects.filter(question=self.id)

    def format_basic_responses(self):
        return format_results.basic(self)

    def has_eligible_answer(self):
        return True if TextAnswer.items.next_available_for_review(self) else False

    def get_response_count(self):
        # Must count by participant, rather than total answers, because some questions allow multiple answers per user.
        count_for_registered_users = self.get_answer_model_class.objects.filter(
            question=self.id
        ).values('respondent').distinct().count()
        count_for_unregistered_participants = self.get_answer_model_class.objects.filter(
            question=self.id, session_key__isnull=False
        ).values('session_key').distinct().count()
        total_participants = count_for_registered_users + count_for_unregistered_participants
        return total_participants


class Code(models.Model):
    text = models.CharField(max_length=24)


class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.PROTECT)
    respondent = models.ForeignKey(Respondent, on_delete=models.SET_NULL, blank=True, null=True)
    session_key = models.CharField(blank=True, null=True, max_length=40)
    moment = models.DateTimeField()

    items = AnswerManager()
    objects = models.Manager()

    class Meta:
        abstract = True

    def get_form(self):
        from Survey.forms import RESPONSE_FORMS
        question_format = self.question.response_format
        return RESPONSE_FORMS[question_format]


class TextAnswer(Answer):
    response = models.TextField(blank=True, null=True)
    code = models.ForeignKey(Code, blank=True, null=True, on_delete=models.PROTECT)


class BooleanAnswer(Answer):
    response = models.BooleanField(null=True)


class ChoiceAnswer(Answer):
    response = models.IntegerField(blank=True, null=True)  # Integer is an index of Answer's Options array.


class RangeAnswer(Answer):
    response = models.IntegerField(blank=True, null=True)


class WeeklyQuestion(models.Model):
    organization = models.ForeignKey("Home.Organization", on_delete=models.CASCADE, null=True)
    text = models.CharField(max_length=64)
    week_of = models.DateField()
    questions = WeeklyQuestionManager()
    objects = models.Manager()

    def __str__(self):
        return "For: {}".format(self.week_of)


class WeeklyAnswer(models.Model):
    question = models.ForeignKey(WeeklyQuestion, on_delete=models.PROTECT)
    text = models.TextField()
    moment = models.DateTimeField()
    respondent = models.ForeignKey(Respondent, null=True, on_delete=models.SET_NULL)


class Translation(models.Model):
    """Stores alternate text to present as translations."""
    title = models.CharField(max_length=48, blank=True, null=True)
    text = models.TextField()
    options = ArrayField(models.CharField(max_length=512), blank=True, null=True)
    language = models.ForeignKey("Home.Language", on_delete=models.PROTECT)
    question = models.ForeignKey(Question, null=True, blank=True, on_delete=models.CASCADE)
    questionnaire = models.ForeignKey(Questionnaire, null=True, blank=True, on_delete=models.CASCADE)
    weekly_question = models.ForeignKey(WeeklyQuestion, null=True, blank=True, on_delete=models.CASCADE)



# @receiver(post_save, sender=Questionnaire)
# def add_new_eligible_users_to_questionnaire(sender, instance, created, **kwargs):
#     if created:
#         if instance.open_to_all:
#             users = Respondent.objects.filter(organization=instance.organization)
#             instance.eligible_users.set(users)

