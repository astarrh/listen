from django.urls import path

from Survey import views as views
from Survey import actions as actions

urlpatterns = [
    path('', views.SurveyAdminListView.as_view(), name="list"),

    path('manage/', views.ManageSurveysApp.as_view(), name="manage"),
    path('builder/<slug:slug>/', views.SurveyBuilderApp.as_view(), name='builder'),
    path('<slug:slug>/stats/', views.SurveyStatsView.as_view(), name='stats'),
    path('<slug:slug>/review/', views.SurveyAnswersView.as_view(), name="review"),
    path('<slug:slug>/review_weekly', views.WeeklyAnswersView.as_view(), name="review_weekly"),
    path('set_permissions/<slug:slug>/', views.ManageAnalysisPermissionsView.as_view(), name="analysis_permissions"),
    path('add_response/<slug:slug>/', views.AddResponseApp.as_view(), name='add_response'),

    path('survey_admin/', actions.survey_admin, name='admin'),
    path('preview_questionnaire/', actions.preview_questionnaire, name='preview_questionnaire'),
    path('save_survey', actions.save_questionnaire, name="save_survey"),
    path('accept_code/', actions.accept_code, name='accept_code'),

    path('get_question/', actions.get_question, name='get_question'),
    path('save_question/', actions.save_question, name="save_question"),
    path('delete_question/', actions.delete_question, name="delete_question"),
    path('move_question/', actions.move_question, name='move_question'),

    path('save_response/', actions.save_response, name="save_response"),
    path('process_manual_response', actions.process_manual_response, name='process_manual_response'),
    path('unlock_survey/', actions.unlock_survey_for_user, name="unlock"),
    path('create_weekly/', actions.create_weekly, name="create_weekly"),
    path('record_weekly/', actions.record_weekly, name="record_weekly"),

    path('auth_respondent/', actions.auth_respondent, name='auth_respondent'),
    path('log_out/', actions.log_out, name='log_out'),

    path('<slug:slug>/', views.TakeSurveyApp.as_view(), name="view"),
]
