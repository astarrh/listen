import uuid
from datetime import datetime
from django.db.models import F, Q

from channels.auth import login, logout
from asgiref.sync import async_to_sync
from dateutil import parser
from json import loads as from_json
from json import dumps as to_json
from channels.generic.websocket import WebsocketConsumer

from Home.models import Language
from Project.models import Project, District
from Survey.utils.survey_responses import process_response
from .utils.tags import extract_ti_tag
from Survey.models import Question, Respondent, Questionnaire, Population, Translation
from Survey import auth


class ResponseConsumer(WebsocketConsumer):

    def connect(self):
        print('connect')
        self.user = self.scope['user']
        self.questionnaire = Questionnaire.objects.get(slug=self.scope['url_route']['kwargs']['slug'])
        self.respondent, _ = Respondent.objects.get_or_create(user=self.user) if self.user.is_authenticated else (None, None)
        if not self.respondent:
            self.session_key = self.scope['session']['key']
        else:
            self.respondent.started.add(self.questionnaire)
        self.accept()
        print(self.user)
        self.send(text_data=to_json({
            'operation': 'set_user',
            'success': True,
            'message': "Loading User Data",
            'object': {'username': self.user.username}
        }))

    def receive(self, text_data):
        print('receive')
        data = self.data = from_json(text_data)
        operation = data.get('operation')
        if operation == 'save_response':
            self.save_response()
        elif operation in ['sign_in', 'sign_up']:
            self.auth_user(operation)
        elif operation == "sign_out":
            async_to_sync(logout)(self.scope)
            self.respondent = None
            self.send(text_data=to_json({
                'operation': 'set_user',
                'success': True,
                'message': "Removing User Data",
            }))
        else:
            self.send(text_data=to_json({
                'operation': False,
                'message': "Unrecognized Operation",
                'errors': {'system': "Unrecognized Operation"}
            }))

    def disconnect(self, close_code):
        print('disconnect')
        # self.respondent.finished.add(self.questionnaire)

    def save_response(self):
        question = Question.objects.get(id=self.data['survey'])
        answer = self.data['response']
        moment = parser.isoparse(self.data['moment'])
        identifier = {'respondent': self.respondent} if self.respondent else {'session_key': self.session_key}
        success, message = process_response(question, answer, moment, **identifier)
        self.send(text_data=to_json({
            'operation': 'save_response',
            'success': success,
            'reference': question.id,
            'message': message
        }))

    def auth_user(self, operation):
        form_data = self.data.get('data')
        auth_method = auth.OPERATIONS[operation]
        self.user = auth_method(form_data)
        survey = Questionnaire.objects.get(slug=form_data.pop('slug'))
        if isinstance(self.user, str):
            self.send(text_data=to_json({
                'operation': 'set_user',
                'success': False,
                'message': self.user,
                'errors': {operation: self.user}})
            )
        else:
            async_to_sync(login)(self.scope, self.user)
            self.scope["session"].save()
            self.respondent = Respondent.objects.get_or_create(user=self.user)
            self.respondent.eligible_for.add(survey)
            self.send(text_data=to_json({
                'operation': 'set_user',
                'success': True,
                'message': "Loading User Data",
                'object': {'username': self.user.username}
            }))


class BuilderConsumer(WebsocketConsumer):

    def connect(self):
        print('connect')
        self.user = self.scope['user']
        self.accept()
        self.send(text_data=to_json({
            'operation': 'set_user',
            'success': True,
            'message': "Loading User Data",
            'object': {'username': self.user.username}
        }))

    def receive(self, text_data):
        print('receive')
        data = self.data = from_json(text_data)
        operation = data.get('operation')
        if operation == 'save_survey':
            self.save_questionnaire()
        elif operation == 'save_question':
            self.save_question()
        elif operation == 'get_question':
            self.get_question()
        elif operation == 'delete_question':
            self.delete_question()
        elif operation == 'move_question':
            self.move_question()
        else:
            self.send(text_data=to_json({
                'operation': False,
                'message': "Unrecognized Operation",
                'errors': {'system': "Unrecognized Operation"}
            }))

    def disconnect(self, close_code):
        print('disconnect')

    def get_question(self):
        pk = self.data.get('pk')
        question = Question.objects.get(id=pk)
        self.send(text_data=to_json({
            'operation': 'get_question',
            'message': 'Question Data Retrieved',
            'object': question.builder_data()
        }))

    def move_question(self):
        question = Question.objects.get(id=self.data.get('pk'))
        new_position = self.data.get('position')
        if new_position > question.position:
            Question.objects.filter(
                questionnaire=question.questionnaire,
                position__gt=question.position,
                position__lte=new_position
            ).update(position=F('position') - 1)
        else:
            Question.objects.filter(
                questionnaire=question.questionnaire,
                position__gte=new_position,
                position__lt=question.position
            ).update(position=F('position') + 1)
        question.position = new_position
        question.save()

    def delete_question(self):
        pk = self.data.get('pk')
        question = Question.objects.get(id=pk)
        position = question.position
        question.delete()
        Question.objects.filter(position__gt=position).update(position=F('position') - 1)

    def save_question(self):
        data = self.data.get('data')
        pk = data.pop('pk')
        data['tags'] = extract_ti_tag(data.get('tags', None))
        data['options'] = extract_ti_tag(data.get('options', None))
        translation = data.pop('translation', None)
        translated_options = extract_ti_tag(data.pop('translatedOptions', None))

        language = Language.objects.get(code='es')
        if pk == 'new':
            questionnaire = data['questionnaire'] = Questionnaire.objects.get(slug=data.pop('slug'))
            data['position'] = Question.objects.filter(questionnaire=questionnaire).count() + 1
            question = Question.objects.create(**data)
            if translation:
                translation = Translation.objects.create(question=Question(id=question.id), text=translation, language=language)
                if translated_options:
                    translation.options = translated_options
                    translation.save()
        else:
            question, created = Question.objects.update_or_create(id=pk, defaults={**data})
            if translation:
                obj, created = Translation.objects.get_or_create(question=Question(id=question.id), language=language)
                obj.text = translation
                if translated_options:
                    obj.options = translated_options
                obj.save()

        self.send(text_data=to_json({
            'operation': 'save_question',
            'message': 'Question Data Saved',
            'object': {'pk': question.id}
        }))

    def save_questionnaire(self):
        data = self.data.get('data')
        slug = data.pop('slug')
        date_range = data.pop('date_range')
        intro_translation = data.pop('intro_translation')
        data['date_opened'] = datetime.fromisoformat(date_range.pop('start').split('T')[0])
        data['date_closed'] = datetime.fromisoformat(date_range.pop('end').split('T')[0])
        if data.pop('district'):
            data['district'] = District.objects.get(id=data.get('district'))
        if data.pop('project'):
            data['project'] = Project.objects.get(id=data.get('project'))
        if data.pop('population'):
            data['population'] = Population.objects.get(id=data.get('population'))

        if slug == 'new':
            survey = Questionnaire.objects.create(**data)
        else:
            Questionnaire.objects.filter(slug=slug).update(**data)
            survey = Questionnaire.objects.get(slug=slug)

        if survey.login_is_required and not survey.survey_code:
            eligible = Respondent.objects.filter(organization=survey.organization)
            survey.eligible_users.set(eligible)
        language = Language.objects.get(code='es')
        translation, _ = survey.translation_set.get_or_create(language=language)
        translation.text = intro_translation
        translation.save()

        return survey


class ManagerConsumer(WebsocketConsumer):

    def connect(self):
        print('connect')
        self.user = self.scope['user']
        self.accept()
        self.send(text_data=to_json({
            'operation': 'set_user',
            'success': True,
            'message': "Loading User Data",
            'object': {'username': self.user.username}
        }))

    def receive(self, text_data):
        print('receive')
        data = self.data = from_json(text_data)
        operation = data.get('operation')
        if operation == 'save_response':
            self.save_response()
        else:
            self.send(text_data=to_json({
                'operation': False,
                'message': "Unrecognized Operation",
                'errors': {'system': "Unrecognized Operation"}
            }))

    def disconnect(self, close_code):
        print('disconnect')
