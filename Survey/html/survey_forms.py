long_text = \
    '<div class="form-group"> \
         <textarea name="response" class="form-control" rows="5">{}</textarea> \
    </div>'

boolean = \
    '<div class="form-check form-check-inline"> \
        <input class="form-check-input" type="radio" name="response" id="inlineRadio1" {} value="2"> \
        <label class="form-check-label" for="inlineRadio1">Yes</label> \
    </div> \
    <div class="form-check form-check-inline"> \
        <input class="form-check-input" type="radio" name="response" id="inlineRadio2" value="3" {} > \
        <label class="form-check-label" for="inlineRadio2">No</label> \
    </div>'

select_range = \
    '<div class ="form-group"> \
        <span class="pull-left">0%</span> \
        <span class="pull-right">100%</span> \
        <input type="range" min="0" max="100" class ="form-control-range" name="response" {}> \
    </div>'

select_multiple = \
    '<div class="checkbox"> \
        <label "font-size=1em"> \
           <input class="form-check-input" type="checkbox" {} value="{}" name="response"> \
               <span class="cr"><i class="cr-icon fa fa-check"></i></span> \
               {} \
          </label> \
    </div>'


def get_data(user, question):
    return question.get_user_response(user), question.options


def text_format(user_response):
    user_response = user_response if user_response else ""
    html = long_text.format(user_response)
    return html


def radio_format(user_response):
    form_settings = ["", ""]
    if user_response:
        print(user_response)
        form_settings = ["checked='checked'", ""] if user_response is True else ["", "checked='checked'"]
    html = boolean.format(*form_settings)
    return html


def choice_format(user_response, options):
    html = '<select name="response" class="form-control form-control-lg"> <option value="new" disabled {}>Select Answer</option>'
    index = 0
    select_disabled = 'selected'
    for option in options:
        selected = ""
        print(index)
        print(user_response)
        print('--')
        if user_response is not None and index == user_response:
            selected = 'selected="selected"'
            select_disabled = ""
        html += '<option value="{}" {}>{}</option>'.format(index, selected, option)
        index += 1
    html += '</select>'
    return html.format(select_disabled)


def range_format(user_response):
    html = select_range.format("value=" + str(user_response)) if user_response else select_range.format("")
    return html


def multi_format(user_response, options):
    html = '<div class="choice-block">'
    index = 0
    for option in options:
        if option:
            checked = "checked" if user_response and index in user_response else ""
            html += select_multiple.format(checked, index, option)
            index += 1
    html += '</div>'
    return html
