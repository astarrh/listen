from Survey import choices


def basic(question):
    html = ""
    if question.response_format == choices.TEXT:
        html = "<ul>"
        for answer in question.textanswer_set.all():
            html += "<li>{}</li>".format(answer.response)
        html += '</ul>'
    elif question.response_format == choices.CHOICE or question.response_format == choices.MULTI:
        index = 0
        html = "<ul>"
        total_responses = question.choiceanswer_set.all().distinct('respondent').count()
        print(total_responses)
        for option in question.options:
            if option:
                number_of_answers = question.choiceanswer_set.filter(response=index).count()
                print(number_of_answers)
                html += "<li>{}: {}% ({})</li>".format(option, ((number_of_answers / total_responses) * 100), number_of_answers)
                index += 1
        html += '</ul>'

    return html
