QUESTIONNAIRE = {
    'label': "",
    'introduction': "",
    'intro_translation': "",
    'population': "",
    'district': "",
    'project': "",
    'survey_code': "",
    'login_is_required': True,
    'date_range': {
        'start': "",
        'end': "",
    }
}