from django.forms import ModelForm, CharField

from Survey.models import TextAnswer, BooleanAnswer, ChoiceAnswer, RangeAnswer, Respondent, WeeklyAnswer, Question
from Survey.choices import SHORT_TEXT, LONG_TEXT, BOOLEAN, CHOICE, RANGE, MULTI


class QuestionForm(ModelForm):
    options = CharField(required=False)
    translations = CharField(required=False)

    class Meta:
        model = Question
        fields = ['questionnaire', 'text', 'response_format']


class TextAnswerForm(ModelForm):
    class Meta:
        model = TextAnswer
        fields = ['response']


class BooleanAnswerForm(ModelForm):
    class Meta:
        model = BooleanAnswer
        fields = ['response']


class ChoiceAnswerForm(ModelForm):
    class Meta:
        model = ChoiceAnswer
        fields = ['response']


class RangeAnswerForm(ModelForm):
    class Meta:
        model = RangeAnswer
        fields = ['response']


class RespondentDemographicsForm(ModelForm):
    class Meta:
        model = Respondent
        fields = ['heritage', 'gender', 'date_of_birth']


class WeeklyAnswerForm(ModelForm):
    class Meta:
        model = WeeklyAnswer
        fields = ['text']


RESPONSE_FORMS = {
    SHORT_TEXT: TextAnswerForm,
    LONG_TEXT: TextAnswerForm,
    BOOLEAN: BooleanAnswerForm,
    CHOICE: ChoiceAnswerForm,
    RANGE: RangeAnswerForm,
    MULTI: ChoiceAnswerForm,
}
