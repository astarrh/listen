import uuid

from json import dumps as to_json
from datetime import datetime

from django.core.serializers.json import DjangoJSONEncoder
from django.views.generic import TemplateView, FormView
from django.views.generic.edit import DeleteView
from django.urls import reverse_lazy

from django.contrib.auth.mixins import UserPassesTestMixin

from Home.models import Organization
from Home.views import LanguageMixin
from Project.models import Project
from Analyze.models import Researcher
from .models import Questionnaire, Respondent, WeeklyQuestion, TextAnswer, Translation
from .choices import RESPONSE_FORMATS
from Survey import structs


class TakeSurveyApp(UserPassesTestMixin, TemplateView):
    template_name = 'survey_app.html'
    app_name = "survey"

    def test_func(self):
        authorized = True
        slug = self.kwargs.get('slug')
        survey = Questionnaire.objects.get(slug=slug)
        if survey.is_closed() or survey.hidden:
            authorized = False
        elif survey.login_is_required:
            if self.request.user.is_authenticated:
                respondent = Respondent.objects.get(user=self.request.user)
                authorized = respondent.eligible_for.filter(id=survey.id).exists()
            else:
                authorized = False
        else:
            if self.request.user.is_authenticated:
                respondent = Respondent.objects.get(user=self.request.user)
                respondent.eligible_for.add(survey)
            else:
                key = self.request.session.session_key
                self.request.session['key'] = key
        return authorized

    def survey_data(self):
        slug = self.kwargs.get('slug')
        survey = Questionnaire.objects.get(slug=slug)
        questions = survey.question_set.all()
        language_code = self.request.LANGUAGE_CODE
        text_translations = {}
        options_translations = {}
        answers = {}
        if self.request.LANGUAGE_CODE == 'en':
            intro = survey.introduction
        else:
            intro = Translation.objects.filter(questionnaire=survey, language__code=language_code).first().text

        for question in questions:

            translation_set = question.translation_set.all().select_related('language')
            for t in translation_set:
                text_translations[question.id] = {t.language.code: t.text}
                if t.options:
                    options_translations[question.id] = {t.language.code: t.options}

            if self.request.user.is_authenticated:
                answers[question.id] = question.get_user_response(self.request.user)
            else:
                session_key = self.request.session.session_key
                answers[question.id] = question.get_anonymous_response(session_key)

        data = {
            "slug": slug,
            "intro": intro,
            "questions": list(questions.values('response_format', 'text', 'options', 'id')),
            "textTranslations": text_translations,
            "optionsTranslations": options_translations,
            "answers": answers
        }
        return to_json(data)


class ManageSurveysApp(UserPassesTestMixin, TemplateView):
    template_name = "manage_app.html"
    app_name = "manageSurveys"

    def test_func(self):
        user = self.request.user
        return True if user.is_admin or user.is_staff else False

    def all_surveys(self):
        data = {
            'past': to_json(list(Questionnaire.available.in_the_past().values(
                'pk', 'label', 'date_opened', 'date_closed', 'slug'
            )), cls=DjangoJSONEncoder),
            'present': to_json(list(Questionnaire.available.today().values(
                'pk', 'label', 'date_opened', 'date_closed', 'slug'
            )), cls=DjangoJSONEncoder),
            'future': to_json(list(Questionnaire.available.in_the_future().values(
                'pk', 'label', 'date_opened', 'date_closed', 'slug'
            )), cls=DjangoJSONEncoder),
        }
        return data


class AddResponseApp(UserPassesTestMixin, TemplateView):
    template_name = "add_response_app.html"
    app_name = "addResponse"

    def test_func(self):
        user = self.request.user
        return True if user.is_admin or user.is_staff else False

    def get_context_data(self, *args, **kwargs):
        context = super(AddResponseApp, self).get_context_data(*args, **kwargs)
        slug = self.kwargs.get('slug')
        questions = Questionnaire.objects.get(slug=slug).question_set.all()
        questions = list(questions.values('response_format', 'text', 'options', 'id'))
        for question in questions:
            if 'text' in question['response_format']:
                question['response'] = []
                question['proposed'] = ""
            elif question['response_format'] == 'select_multi':
                question['response'] = []
            elif question['response_format'] == 'range':
                question['response'] = 50
            else:
                question['response'] = None
        context['questions'] = to_json(questions)
        return context


class SurveyBuilderApp(UserPassesTestMixin, TemplateView):
    template_name = "builder_app.html"
    app_name = "editSurvey"

    def test_func(self):
        user = self.request.user
        return True if user.is_admin or user.is_staff else False

    def get_context_data(self, *args, **kwargs):
        context = super(SurveyBuilderApp, self).get_context_data(*args, **kwargs)
        slug = self.kwargs.get('slug')

        if slug == 'new':
            data = structs.QUESTIONNAIRE
            questions = {}
        else:
            survey = Questionnaire.objects.get(slug=slug)
            data = survey.builder_data()
            question_objs = survey.question_set.all()
            questions = []
            for obj in question_objs:
                questions.append({
                    'pk': obj.id,
                    'text': obj.text,
                    'response_format': obj.get_response_format_display()
                })
        data['slug'] = slug

        context['surveyData'] = to_json(data, cls=DjangoJSONEncoder)
        context['questions'] = questions
        context['response_options'] = {opt: label for opt, label in RESPONSE_FORMATS}
        context['organization'] = Organization.objects.get(name="Listen To Shafter").id

        return context


class SurveyAdminListView(UserPassesTestMixin, LanguageMixin, TemplateView):
    template_name = "questionnaire_list.html"

    def test_func(self):
        permitted = False
        if self.request.user.is_admin or self.request.user.is_staff:
            permitted = True
        return permitted

    @property
    def organization(self):
        return self.request.user.organization

    def unpublished(self):
        return Questionnaire.available.in_the_future().filter(organization=self.organization)

    def current(self):
        return Questionnaire.available.today().filter(organization=self.organization)

    def completed(self):
        return Questionnaire.available.in_the_past().filter(organization=self.organization)

    def weekly_questions(self):
        return WeeklyQuestion.objects.filter(organization=self.request.user.organization).order_by('-week_of')

    def newsletter_date(self):
        return datetime.today().strftime('%B-%Y').lower()

    def projects(self):
        return Project.objects.filter(organization=self.request.user.organization)


class SurveyStatsView(UserPassesTestMixin, TemplateView):
    template_name = 'survey_stats.html'

    def test_func(self):
        permitted = False
        if self.request.user.is_admin or self.request.user.is_staff:
            permitted = True
        return permitted

    def survey(self):
        return Questionnaire.objects.get(id=self.kwargs.get('pk'))

    def all_responses(self):
        survey = self.survey()
        return TextAnswer.items.count_users_responded_between(survey, survey.date_opened, datetime.now())

    def all_completions(self):
        survey = self.survey()
        return TextAnswer.items.count_users_finished_between(survey, survey.date_opened, datetime.now())


class SurveyBuilderView(UserPassesTestMixin, LanguageMixin, TemplateView):
    template_name = "survey_builder.html"
    hide_options = [1, 2, 4]

    def survey(self):
        return Questionnaire.objects.get(id=self.kwargs.get('pk'))

    def test_func(self):
        user = self.request.user
        permitted = False
        if user.is_admin or (user.is_staff and user.organization == self.survey().organization):
            permitted = True
        return permitted


class SurveyAnswersView(UserPassesTestMixin, LanguageMixin, TemplateView):
    template_name = "answers.html"

    def survey(self):
        return Questionnaire.objects.get(id=self.kwargs.get('pk'))

    def test_func(self):
        user = self.request.user
        permitted = False
        if user.is_admin or (user.is_staff and user.organization == self.survey().organization):
            if not self.survey().qualitative:
                permitted = True
        return permitted


class WeeklyAnswersView(UserPassesTestMixin, TemplateView):
    template_name = "weekly_answers.html"

    def question(self):
        return WeeklyQuestion.objects.get(id=self.kwargs.get('pk'))

    def answers(self):
        return self.question().weeklyanswer_set.all().order_by('-id')

    def test_func(self):
        user = self.request.user
        permitted = False
        if user.is_admin or (user.is_staff and user.organization) == self.question().organization:
            permitted = True
        return permitted


def activate_survey(request):
    survey = Questionnaire.objects.get(id=request.POST.get('pk')).delete()
    survey.date_opened = datetime.today()
    survey.closed = False


class DeleteSurveyView(DeleteView):
    model = Questionnaire
    success_url = reverse_lazy('Surveys:survey_list')


class ManageAnalysisPermissionsView(TemplateView, UserPassesTestMixin):
    template_name = "analysis_permissions.html"

    def test_func(self):
        permitted = False
        if self.request.user.is_admin or self.request.user.is_staff:
            permitted = True
        return permitted

    def survey(self):
        return Questionnaire.objects.get(id=self.kwargs.get('pk'))

    def researchers(self):
        response_dict = {1: [], 2: [], 3: [], 4: [], 5: []}
        for researcher in Researcher.objects.filter(user__organization=self.request.user.organization):
            level = researcher.calculate_current_competency()
            if level < 6:
                response_dict[level].append(researcher)
        return response_dict
