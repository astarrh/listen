from django.contrib import admin
from Survey import models

# Register your models here.
admin.site.register(models.Questionnaire)
admin.site.register(models.Question)
admin.site.register(models.Respondent)
admin.site.register(models.TextAnswer)
admin.site.register(models.ChoiceAnswer)
admin.site.register(models.BooleanAnswer)
admin.site.register(models.RangeAnswer)
admin.site.register(models.WeeklyQuestion)
admin.site.register(models.WeeklyAnswer)
admin.site.register(models.Translation)
