from random import choice
from datetime import date, datetime
from django.db.models import Manager, Q, Count


class WeeklyQuestionManager(Manager):

    def current(self):
        today = date.today()
        current_week = today.isocalendar()[1]
        current_year = today.isocalendar()[0]

        # Adjust for Python's week starting on Monday
        if today.weekday() == 6:
            current_week += 1

        current = self.model.objects.filter(week_of__week=current_week, week_of__year=current_year)

        # Won't fail if latest is from a previous year, but the template may still have issues if nothing exists at all.
        if current.exists():
            result = current.latest('week_of')
        else:
            most_recent = self.model.objects.filter(week_of__week__lt=current_week, week_of__year=current_year)
            result = most_recent.latest('week_of') if most_recent.exists() else self.model.objects.latest('week_of')

        return result


class QuestionnaireManager(Manager):

    def today(self):
        today = datetime.now().date()
        available = self.model.objects.filter(
            Q(date_closed__gt=today) | Q(date_closed__isnull=True),
            date_opened__lte=today,
            hidden=False
        )
        return available

    def in_the_past(self):
        today = datetime.now().date()
        available = self.model.objects.filter(
            Q(date_closed__lte=today),
            hidden=False
        ).order_by('-date_closed')
        return available

    def in_the_future(self):
        today = datetime.now().today()
        available = self.model.objects.filter(
            Q(date_opened__gt=today) | Q(date_opened__isnull=True),
            hidden=False
        )
        return available


class AnswerManager(Manager):

    def next_available_for_review(self, question):
        answer = self.model.objects.filter(
            question=question, resolution__isnull=True
        ).annotate(
            qty_evaluations=Count('evaluations')
        ).order_by(
            '-qty_evaluations'
        ).first()

        return answer

    def next_available_for_review_per_user(self, question, researcher):
        answer = self.model.objects.filter(
            question=question, resolution__isnull=True
        ).exclude(
            evaluations__researcher__in=[researcher.id]
        ).annotate(
            qty_evaluations=Count('evaluations')
        ).order_by(
            '-qty_evaluations'
        ).first()

        return answer

    def count_users_responded_between(self, survey, begin, end):
        results = self.model.objects.filter(
            question__questionnaire=survey,
            moment__gt=begin,
            moment__lt=end
        ).values('respondent').distinct().count()

        return results

    def count_users_finished_between(self, survey, begin, end):
        results = self.model.objects.filter(
            question__questionnaire=survey,
            respondent__finished__in=[survey],
            moment__gt=begin,
            moment__lt=end
        ).values('respondent').distinct().count()

        return results