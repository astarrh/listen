from json import loads as unpack
from datetime import datetime, timedelta
from operator import itemgetter
from .utils.tags import extract_ti_tag
from dateutil import parser

from django.contrib.auth import logout, login
from django.http import JsonResponse
from django.utils import timezone
from django.urls import reverse_lazy
from django.db.models import F, ProtectedError
from django.core.exceptions import ObjectDoesNotExist


from Home.models import Language, ListenUser
from Project.models import Project, District
from Home.tools import update_session
from Survey.utils.survey_responses import process_response, process_response_code

from .models import Respondent, Question, WeeklyQuestion, Questionnaire, ChoiceAnswer, Translation, Population
from .forms import RespondentDemographicsForm, WeeklyAnswerForm
from .choices import MULTI

from Survey import auth
from Survey.models import Respondent


def get_question(request):
    response = {'success': False}
    if request.method != "POST":
        return JsonResponse(response)

    form_data = unpack(request.body)
    pk = form_data.get('pk')
    question = Question.objects.get(id=pk)
    response = {
        'operation': 'get_question',
        'message': 'Question Data Retrieved',
        'object': question.builder_data()
    }
    return JsonResponse(response)


def move_question(request):
    response = {'success': False}
    if request.method != "POST":
        return JsonResponse(response)

    form_data = unpack(request.body)
    question = Question.objects.get(id=form_data.get('pk'))
    new_position = form_data.get('position')
    if new_position > question.position:
        Question.objects.filter(
            questionnaire=question.questionnaire,
            position__gt=question.position,
            position__lte=new_position
        ).update(position=F('position') - 1)
    else:
        Question.objects.filter(
            questionnaire=question.questionnaire,
            position__gte=new_position,
            position__lt=question.position
        ).update(position=F('position') + 1)
    question.position = new_position
    question.save()

    return JsonResponse({'success': True, 'operation':'move_question'})


def delete_question(request):
    response = {'success': False}
    if request.method != "POST":
        return JsonResponse(response)

    pk = request.body.get('pk')
    question = Question.objects.get(id=pk)
    position = question.position
    question.delete()
    Question.objects.filter(position__gt=position).update(position=F('position') - 1)
    return JsonResponse({'success': True, 'operation': 'delete_question'})


def save_question(request):
    response = {'success': False}
    if request.method != "POST":
        return JsonResponse(response)

    form_data = unpack(request.body).get('data')
    pk = form_data.pop('pk')
    form_data['tags'] = extract_ti_tag(form_data.get('tags', None))
    form_data['options'] = extract_ti_tag(form_data.get('options', None))
    translation = form_data.pop('translation', None)
    translated_options = extract_ti_tag(form_data.pop('translatedOptions', None))

    language = Language.objects.get(code='es')
    if pk == 'new':
        questionnaire = form_data['questionnaire'] = Questionnaire.objects.get(slug=form_data.pop('slug'))
        form_data['position'] = Question.objects.filter(questionnaire=questionnaire).count() + 1
        question = Question.objects.create(**form_data)
        if translation:
            translation = Translation.objects.create(question=Question(id=question.id), text=translation,
                                                     language=language)
            if translated_options:
                translation.options = translated_options
                translation.save()
    else:
        question, created = Question.objects.update_or_create(id=pk, defaults={**form_data})
        if translation:
            obj, created = Translation.objects.get_or_create(question=Question(id=question.id), language=language)
            obj.text = translation
            if translated_options:
                obj.options = translated_options
            obj.save()

    response = {
        'operation': 'save_question',
        'message': 'Question Data Saved',
        'object': {'pk': question.id}
    }
    return JsonResponse(response)


def process_manual_response(request):
    response = {'success': False, 'message':"Incorrect Request."}
    if request.method != "POST":
        return JsonResponse(response)
    question_data = unpack(request.body).get('data')
    print(question_data)
    for key, item in question_data.items():
        print('smoke: ' + key)
        answer = item.get('response')
        if answer:
            print(answer)
            question = Question.objects.get(id=item.get('id'))
            moment = datetime.utcnow()
            session_key = str(request.user.id) + '-' + str(moment.timestamp())
            if 'text' in item.get('response_format'):
                success, message = process_response_code(question, answer, moment, session_key)
            else:
                success, message = process_response(question, answer, moment, **{'session_key': session_key})
            response = {
                'operation': 'save_response',
                'success': success,
                'reference': question.id,
                'message': message
            }
    return JsonResponse(response)


def preview_questionnaire(request):
    response = {'success': False}
    if request.method != "POST":
        return JsonResponse(response)

    form_data = unpack(request.body)
    survey = Questionnaire.objects.get(id=form_data.get('pk'))
    avg_response = survey.average_response_rate()
    response = {
        'success': True,
        'results': {
            'title': survey.label,
            'responseRate': avg_response,
            'start': survey.date_opened.strftime("%x"),
            'end': survey.date_closed.strftime("%x"),
        }
    }
    return JsonResponse(response)


def save_questionnaire(request):
    response = {'success': False}
    if request.method != "POST":
        return JsonResponse(response)

    form_data = unpack(request.body).get('data')
    slug = form_data.pop('slug')
    date_range = form_data.pop('date_range')
    intro_translation = form_data.pop('intro_translation')
    form_data['date_opened'] = datetime.fromisoformat(date_range.pop('start').split('T')[0])
    form_data['date_closed'] = datetime.fromisoformat(date_range.pop('end').split('T')[0])
    if form_data.pop('district'):
        form_data['district'] = District.objects.get(id=form_data.get('district'))
    if form_data.pop('project'):
        form_data['project'] = Project.objects.get(id=form_data.get('project'))
    if form_data.pop('population'):
        form_data['population'] = Population.objects.get(id=form_data.get('population'))

    if slug == 'new':
        survey = Questionnaire.objects.create(**form_data)
    else:
        Questionnaire.objects.filter(slug=slug).update(**form_data)
        survey = Questionnaire.objects.get(slug=slug)

    if survey.login_is_required and not survey.survey_code:
        eligible = Respondent.objects.filter(organization=survey.organization)
        survey.eligible_users.set(eligible)
    language = Language.objects.get(code='es')
    translation, _ = survey.translation_set.get_or_create(language=language)
    translation.text = intro_translation
    translation.save()

    response = {
        'operation': 'save_survey',
        'message': 'Survey Data Saved',
        'object': {'pk': survey.id, 'slug': survey.slug}
    }
    return JsonResponse(response)


def accept_code(request):
    response = {'success': False}
    if request.method != "POST":
        return JsonResponse(response)

    code = request.POST.get('code')
    code = code.upper()
    try:
        slug = Questionnaire.objects.get(survey_code=code).slug
        url = reverse_lazy('Survey:view', args=[slug])
        response['target'] = url
        response['success'] = True
    except ObjectDoesNotExist:
        if request.LANGUAGE_CODE == 'es':
            response['message'] = "Sorry, but our system isn't recognizing this code right now. Please check the spelling and send an email to contact@listentoshafter.org"
        else:
            response['message'] = "Lo siento..."
    return JsonResponse(response)


def save_response(request):
    response = {'success': False}
    if request.method != "POST":
        return JsonResponse(response)

    form_data = unpack(request.body)
    question = Question.objects.get(id=form_data['survey'])
    answer = form_data['response']
    moment = parser.isoparse(form_data['moment'])
    respondent = Respondent.objects.get(user=request.user) if request.user.is_authenticated else None
    identifier = {'respondent': respondent} if respondent else {'session_key': request.session.session_key}
    success, message = process_response(question, answer, moment, **identifier)
    response = {
        'operation': 'save_response',
        'success': success,
        'reference': question.id,
        'message': message
    }
    return JsonResponse(response)


def record_weekly(request):
    response = {'success': False}
    if request.method != "POST":
        return JsonResponse(response)

    form = WeeklyAnswerForm(request.POST)
    if form.is_valid():
        answer = form.save(commit=False)
        answer.moment = timezone.now()
        answer.question = WeeklyQuestion.questions.current()
        answer.respondent = None if request.user.is_anonymous else Respondent.objects.get(user=request.user)
        answer.save()
        update_session(request.session)
        response = {'success': True}
    else:
        print(form.errors)

    return JsonResponse(response)


def unlock_survey_for_user(request):
    if request.method != "POST":
        return False

    code = request.POST.get('code')
    respondent = Respondent.objects.get(user=request.user)

    try:
        survey = Questionnaire.objects.get(unlock_code=code, organization=respondent.organization)
        if survey in respondent.eligible_for.all():
            response = {'success': False, 'message': 'You already have access to this survey.'}
        else:
            respondent.eligible_for.add(survey)
            response = {'success': True, 'message': 'success'}
    except ObjectDoesNotExist:
        response = {'success': False, 'message': 'Code not recognized.'}
    return JsonResponse(response)


def create_weekly(request):
    if request.method != "POST":
        response = {'success': False, 'message': "Request Denied"}
    elif request.user.is_admin or request.user.is_staff:
        text, date = itemgetter('text', 'date')(dict(request.POST))

        # User Submits Sunday, but Python weeks begin on Monday
        date = datetime.strptime(date[0], "%m/%d/%Y") + timedelta(days=1)

        weekly_question = WeeklyQuestion.objects.create(
            organization=request.user.organization,
            text=text[0],
            week_of=date
        )

        language = Language.objects.get(label="Spanish")

        Translation.objects.create(
            text=request.POST.get('translation'),
            language=language,
            weekly_question=weekly_question
        )

        response = {'success': True}
    else:
        response = {'success': False, 'message': "Insufficient Permissions"}

    return JsonResponse(response)


def survey_admin(request):
    if request.method != "POST":
        return False

    action = request.POST.get('action')
    survey = request.POST.get('survey')

    survey = Questionnaire.objects.get(id=survey)
    if action == "delete":
        survey.delete()
    elif action == "copy":
        survey.duplicate()
    else:
        if action == "activate":
            survey.date_opened = datetime.today()
            survey.closed = False
        elif action == "close":
            survey.date_closed = datetime.today()
            survey.closed = True
        elif action == 'archive':
            survey.visible = False
        survey.save()

    response = {'success': True, 'message': 'success'}
    return JsonResponse(response)


def auth_respondent(request):
    response = {'success': False}
    if request.method != "POST":
        return JsonResponse(response)

    request_data = unpack(request.body)
    print(request_data)
    operation = request_data.get('operation')
    form_data = request_data.get('data')
    auth_method = auth.OPERATIONS[operation]
    user = auth_method(form_data)
    survey = Questionnaire.objects.get(slug=form_data.pop('slug'))
    if isinstance(user, str):
        response = {
            'operation': 'set_user',
            'success': False,
            'message': user,
            'errors': {operation: user}
        }
    else:
        login(request, user)
        respondent, created = Respondent.objects.get_or_create(user=ListenUser(id=user.id))
        respondent.eligible_for.add(survey)
        response = {
            'operation': 'set_user',
            'success': True,
            'message': "Loading User Data",
            'object': {'username': user.username}
        }
    return JsonResponse(response)


def log_out(request):
    logout(request)
    return JsonResponse({'success': True, "operation": 'log_out'})
