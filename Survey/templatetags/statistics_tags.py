from datetime import datetime, timedelta

from django import template

from Survey.models import TextAnswer

register = template.Library()


@register.filter
def recent_responses(survey, hours):
    now = datetime.now()
    begin = now - timedelta(hours=int(hours))
    return TextAnswer.items.count_users_responded_between(survey, begin, now)


@register.filter
def recent_completions(survey, hours):
    now = datetime.now()
    begin = now - timedelta(hours=int(hours))
    return TextAnswer.items.count_users_finished_between(survey, begin, now)

