from django import template

from ..models import Translation
from ..html.survey_forms import text_format, radio_format, choice_format, range_format, multi_format
from ..choices import LONG_TEXT, BOOLEAN, CHOICE, RANGE, MULTI

# from Home.models import Language

register = template.Library()


@register.simple_tag
def generate_form(user, question):
    """
    This tag generates an HTML form with a single field based on the format requirements of the Question object.
    If an Answer object exists for the user, it will be included as the default value for the field. Most of the
    actual work is done in html.survey_forms.py
    :param user: Logged in user
    :param question: Question object that the form is being generated for
    :return: A full block of HTML to be rendered in the template.
    """
    html = '<form data-question_id="{}">'.format(question.id)
    user_response = question.get_user_response(user)
    rformat = question.response_format

    if user.survey_language:
        options = Translation.objects.get(question=question, language=user.survey_language).options
    else:
        options = question.options

    if rformat == LONG_TEXT:
        html += text_format(user_response)
    elif rformat == BOOLEAN:
        html += radio_format(user_response)
    elif rformat == CHOICE:
        html += choice_format(user_response, options)
    elif rformat == MULTI:
        html += multi_format(user_response, options)
    elif rformat == RANGE:
        html += range_format(user_response)

    html += '</form>'
    return html


@register.filter
def translate(question, language):
    try:
        translation = Translation.objects.get(question=question, language=language).text
    except Translation.DoesNotExist:
        translation = ""
    return translation


@register.filter
def translate_survey(survey, language):
    try:
        translation = Translation.objects.get(questionnaire=survey, language=language).text
    except Translation.DoesNotExist:
        translation = ""
    return translation


@register.filter
def translate_options(question, language):
    translation = ""
    try:
        translation_obj = Translation.objects.get(question=question, language=language)
        if translation_obj.options:
            for option in translation_obj.options:
                translation += '<input type="text" class="addOptionInput" value="{}" data-lang="{}" style="display:none">'.format(
                    option, language.id
                )
    except Translation.DoesNotExist:
        pass
    return translation
