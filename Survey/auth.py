from django.contrib.auth.hashers import check_password
from django.core.exceptions import ValidationError
from django.contrib.auth.password_validation import validate_password

from Home.models import ListenUser


def validate_data(form_data):
    if ListenUser.objects.filter(username=form_data.get('username')).exists():
        result = "This account already exists."
    elif form_data['password'] != form_data['password2']:
        result = "The passwords do not match."
    else:
        try:
            validate_password(form_data['password'])
            result = "success"
        except ValidationError as err:
            result = err.messages[0]
    return result


def signup(form_data):
    validation_result = validate_data(form_data)
    if validation_result == 'success':
        user = ListenUser.objects.create_user(username=form_data['username'], password=form_data['password'])
        return user
    else:
        return validation_result


def login(form_data):
    try:
        user = ListenUser.objects.get(username=form_data['username'])
    except ListenUser.DoesNotExist:
        return "This account doesn't exist."
    password = form_data['password']
    if check_password(password, user.password):
        return user
    else:
        return "Incorrect Password."


OPERATIONS = {
    'sign_up': signup,
    'sign_in': login
}
