FEMALE = 1
MALE = 2
NON_BINARY = 3
OTHER = 4

GENDERS = (
    (FEMALE, "Female"),
    (MALE, "Male"),
    (OTHER, "Other"),
    (NON_BINARY, "Non-Binary")
)

SHORT_TEXT = 'text_short'
LONG_TEXT = 'text_long'
BOOLEAN = 'boolean'
RANGE = 'range'
CHOICE = 'select_one'
MULTI = 'select_multi'

RESPONSE_FORMATS = (
    (SHORT_TEXT, "Text (Short)"),
    (LONG_TEXT, "Text (Long)"),
    (BOOLEAN, "True/False"),
    (RANGE, "Rate 1-10"),
    (CHOICE, "Multiple Choice"),
    (MULTI, "Multiple Select")
)

MODEL_STRINGS = {
    SHORT_TEXT: "textanswer",
    LONG_TEXT: "textanswer",
    BOOLEAN: "booleananswer",
    CHOICE: "choiceanswer",
    RANGE: "rangeanswer",
    MULTI: "choiceanswer"
}