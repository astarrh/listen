from django.core.mail import send_mass_mail
from django.core.validators import validate_email
from django.core.exceptions import ValidationError

from mail_factory import factory
from mail_factory.mails import BaseMail

class WelcomeEmail(BaseMail):
    template_name = 'welcome'
    params = ['site_name', 'site_url']

factory.register(WelcomeEmail)


def covid_email_invite():
    from Home.models import ListenUser, Organization
    subject = "Listen To Shafter - COVID Community Study"
    message = """Hello Neighbor!
    
Listen To Shafter has put together a community study to assess how local residents are affected by the COVID-19 outbreak and we need you to help us with our research!
    
Please take our anonymous at shaftersurvey.org. Simply create an account if you don't have one already and once you are logged in, the survey will be available from your dashboard. If you have any problems accessing it, please reply to this email.

Your participation will help us better understand how our community is dealing with the life-altering effects of the COVID-19 outbreak and will help us and our partners know how to respond. Please encourage your friends and family to participate as well. We need everybody's help!

Thanks very much,
-Your neighbors at Listen to Shafter"""

    organization = Organization.objects.get(name="Listen To Shafter")
    recipients = ListenUser.objects.filter(organization=organization, email__isnull=False, allows_contact_email=True)

    recipient_addresses = []
    emails = []

    for recipient in recipients:
        try:
            validate_email(recipient.email)
            recipient_addresses.append(recipient.email)
            emails.append((subject, message, 'Listen To Shafter', [recipient.email]))
        except ValidationError:
            pass
    for line in open('newsletter/addresses.csv', newline='\r\n'):
        email = line.strip('\r\n')
        if email not in recipient_addresses:
            emails.append((subject, message, 'Listen To Shafter', [email]))

    return tuple(emails)


def send_all_mails(emails):
    return send_mass_mail(emails)
