{% load i18n %}++++++++++++++++++++

{{ newsletter.title }}: {{ message.title }}

++++++++++++++++++++

{% for article in message.articles.all %}{% if article.title == 'intro' %}{{ article.text|striptags|safe }}{% endif %}{% endfor %}


What we're working on:
======================
{% for article in message.articles.all %}{% if article.title == 'first' %}
{{ article.text|striptags|safe }}
{% endif %}{% endfor %}

What we're inspired by:
=======================

{% for article in message.articles.all %}{% if article.title == 'second' %}
{{ article.text|striptags|safe }}
{% endif %}{% endfor %}

What we're dreaming of:
======================
{% for article in message.articles.all %}{% if article.title == 'third' %}
{{ article.text|striptags|safe }}
{% endif %}{% endfor %}


Thanks for reading! Be sure to reach out if you see anything here that you are even a little bit interested in getting involved with.

Also, please consider supporting Listen To Shafter with a monthly donation via our website!

++++++++++++++++++++

{% trans "Unsubscribe:" %} http://{{ site }}{% url "newsletter_unsubscribe_request" newsletter.slug %}
