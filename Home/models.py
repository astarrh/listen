from django.db import models

from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db.models.signals import post_save
from django.contrib.auth.signals import user_logged_in
from django.dispatch import receiver

from Survey.models import Respondent, Questionnaire
from Analyze.models import Researcher

from .choices import LOCATIONS
from .mails import WelcomeEmail


class Language(models.Model):
    label = models.CharField(max_length=24)
    code = models.CharField(max_length=7)

    def __str__(self):
        return self.label


class Organization(models.Model):
    name = models.CharField(max_length=255)
    host = models.CharField(max_length=255)
    languages = models.ManyToManyField(Language)
    city = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class ListenUserManager(BaseUserManager):

    def create_user(self, username, password=None):

        user = self.model(
            username=username,
        )
        user.set_password(password)
        user.organization = Organization.objects.all().first()
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            username,
            password=password,
        )
        user.is_superuser = True
        user.is_admin = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class ListenUser(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=255, unique=True)
    organization = models.ForeignKey(Organization, on_delete=models.PROTECT, blank=True, null=True)
    email = models.EmailField(verbose_name='email address', max_length=255, null=True, blank=True)
    phone_number = models.CharField(max_length=24, null=True, blank=True)
    allows_contact_email = models.BooleanField(default=False)
    allows_contact_sms = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    login_is_email = models.BooleanField(default=True)
    site_language = models.ForeignKey(Language, null=True, blank=True, on_delete=models.SET_NULL, related_name='users_site')
    survey_language = models.ForeignKey(Language, null=True, blank=True, on_delete=models.SET_NULL, related_name='users_survey')

    objects = ListenUserManager()

    USERNAME_FIELD = 'username'

    def save(self, **kwargs):
        self.organization = Organization.objects.get(name="Listen To Shafter")
        super(ListenUser, self).save(**kwargs)

    @property
    def formatted_phone_number(self):
        raw_number = self.phone_number
        area_code = None
        if len(raw_number) == 10:
            area_code = "({})".format(raw_number[:3])
            raw_number = raw_number[3:]
        if len(raw_number) == 7:
            area_code = area_code if area_code else "(661)"
            output = "{} {}-{}".format(area_code, raw_number[:3], raw_number[3:])
        else:
            output = ""
        return output

    def get_full_name(self):
        return "Fellow Shafter Person"

    def is_researcher(self):
        return True if Researcher.objects.filter(user=self).exists() else False


class Whisper(models.Model):
    data = models.CharField(max_length=40)

    def __str__(self):
        return self.data


class Newsletter(models.Model):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    date = models.DateField()
    intro = models.TextField()

    def __str__(self):
        return self.date


class Bullet(models.Model):
    newsletter = models.ForeignKey(Newsletter, on_delete=models.CASCADE)
    section = models.IntegerField()
    text = models.TextField()
    order = models.IntegerField()

    def __str__(self):
        return "{}, {} ({})".format(self.newsletter, self.section, self.order)

    class Meta:
        ordering = ['order']


class Copy(models.Model):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    header = models.CharField(max_length=87, blank=True, null=True)
    text = models.TextField()
    location = models.IntegerField(choices=LOCATIONS, null=True, blank=True)
    order = models.IntegerField(blank=True, null=True)
    fa_icon = models.CharField(max_length=54, blank=True, null=True)
    event = models.ForeignKey('Project.Event', null=True, on_delete=models.SET_NULL)

    class Meta:
        ordering = ["order"]

    def __str__(self):
        return "{} ({})".format(self.get_location_display(), self.order)


class TranslatedText(models.Model):
    language = models.ForeignKey(Language, on_delete=models.PROTECT)
    text = models.TextField()
    header = models.CharField(max_length=87, blank=True, null=True)
    newsletter = models.ForeignKey(Newsletter, blank=True, null=True, on_delete=models.CASCADE)
    bullet = models.ForeignKey(Bullet, blank=True, null=True, on_delete=models.CASCADE)
    copy = models.ForeignKey(Copy, blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self):
        if self.newsletter:
            output = self.newsletter.date
        elif self.bullet:
            output = "{} ({})".format(self.bullet.newsletter.date, self.bullet.section)
        else:
            output = "{} ({})".format(self.copy.get_location_display(), self.copy.order)

        return output


@receiver(post_save, sender=ListenUser)
def process_user(sender, instance, created, **kwargs):
    if created:

        # Create Respondent Object
        respondent = Respondent.objects.create(user=instance, organization=instance.organization)

        # Set Contact Info
        username = instance.username
        if "@" in username:
            instance.email = username
            instance.save()
        elif username.isdigit():
            if len(username) == 11 and username[0] == "1":
                instance.phone_number = username[1:]
            elif len(username) == 10:
                instance.phone_number = username
            elif len(username) == 7:
                instance.phone_number = "661" + username
            instance.save()

        # Add to Eligible Surveys
        surveys = Questionnaire.objects.filter(
            organization=instance.organization,
            login_is_required=True,
            survey_code__isnull=False
        )
        respondent.eligible_for.set(surveys)

        # Send a welcome email
        if instance.email:
            msg = WelcomeEmail({
                'site_name': "Listen To Shafter",
                'site_url': "http://www.listentoshafter.org"})
            msg.send([instance.email])


@receiver(user_logged_in)
def test(request, user, *args, **kwargs):
    try:
        if request.session.get('language') == 1:
            user.site_language = user.survey_language = Language.objects.get(id=1)
            user.save()
    except AttributeError:
        pass