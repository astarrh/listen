HOME_UPPER = 1
HOME_LOWER = 2
HOME_RESPONSE = 3
DONATE_BULLET = 4
DONATE_P = 5
DONATE_ADDRESS = 6
SIGN_UP_HEADLINE = 7
SIGN_UP_BULLET = 8
SIGN_UP_P = 9

LOCATIONS = (
    (HOME_UPPER, "Home Page, Upper Section"),
    (HOME_LOWER, "Home Page, Lower Section"),
    (HOME_RESPONSE, "Home Page, Weekly Response"),
    (DONATE_BULLET, "Donate Page, Bullets"),
    (DONATE_P, "Donate Page, Paragraphs"),
    (DONATE_ADDRESS, "Donate Page, Address"),
    (SIGN_UP_HEADLINE, "Sign Up Page, Headline"),
    (SIGN_UP_BULLET, "Sign Up Page, Bullets"),
    (SIGN_UP_P, "Sign Up Page, Paragraph"),
)
