from django.template import Library
from Survey.models import Translation

register = Library()


@register.filter
def translate_weekly(obj, language):
    output = obj.text
    if language and language != 'en':
        try:
            output = Translation.objects.get(weekly_question=obj, language__code=language).text
        except Translation.DoesNotExist:
            pass
    return output
