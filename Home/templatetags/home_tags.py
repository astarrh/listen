from django.template import Library

from Home.models import TranslatedText
from Survey.models import Translation

register = Library()


@register.filter
def translate_copy(copy, language):
    output = copy.text
    if language and language != 0:
        try:
            output = TranslatedText.objects.get(copy=copy, language=language).text
        except TranslatedText.DoesNotExist:
            pass
    return output


@register.filter
def translate_copy_headline(copy, language):
    output = copy.header
    if language and language != 0:
        try:
            output = TranslatedText.objects.get(copy=copy, language=language).header
        except TranslatedText.DoesNotExist:
            pass
    return output


@register.filter
def translate_weekly(obj, language):
    output = obj.text

    if language and language != 0:
        try:
            output = Translation.objects.get(weekly_question=obj, language=language).text
        except Translation.DoesNotExist:
            pass
    return output


@register.filter
def translate_label(obj, language):
    output = obj.label

    if language and language != 0:
        try:
            output = Translation.objects.get(questionnaire=obj, language=language).title
        except Translation.DoesNotExist:
            pass
    return output
