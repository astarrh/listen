from django.contrib import admin

from .models import Organization, ListenUser, Whisper, Language, Copy, TranslatedText

admin.site.register(Organization)
admin.site.register(ListenUser)
admin.site.register(Whisper)
admin.site.register(Language)
admin.site.register(Copy)
admin.site.register(TranslatedText)