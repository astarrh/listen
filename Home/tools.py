import datetime
import json
import dateutil.relativedelta as rd
import dateutil.parser as parser

from django.core.serializers.json import DjangoJSONEncoder


def update_session(session):
    today = datetime.datetime.now().replace(hour=11, minute=59)
    delta = rd.relativedelta(weekday=rd.FR)
    next_friday = today + delta
    session_var = json.dumps(next_friday, cls=DjangoJSONEncoder)
    session['next_question'] = session_var


def check_session(session):
    next_question = session.get('next_question')
    return True if next_question and parser.parse(json.loads(next_question)) > datetime.datetime.now() else False
