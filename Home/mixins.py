from Home.models import Organization


class LanguageMixin(object):
    """ Must be added before Generic View"""

    def get_language(self):
        if self.request.user.is_anonymous:
            try:
                language = self.request.session.get('language')
            except AttributeError:
                language = 0
        else:
            try:
                language = self.request.user.site_language.id
            except AttributeError:
                language = None
        return language

    def get_organization(self):
        return Organization.objects.get(host=self.request.get_host())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["language"] = self.get_language()
        return context
