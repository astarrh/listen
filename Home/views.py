from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import TemplateView, FormView, CreateView
from django.contrib.auth.views import LoginView
from django.utils.translation import gettext as _
from django.urls import reverse_lazy
from django.contrib.auth import logout
from django.shortcuts import redirect

from Survey.models import Respondent, Questionnaire, WeeklyQuestion

from Home import choices
from Home.models import ListenUser
from Analyze.models import Researcher
from Blog.models import Post
from newsletter.models import Message, Newsletter, Article
from newsletter.views import SubmissionArchiveDetailView
from .models import Copy
from .forms import CustomUserCreationForm


class LanguageMixin(object):
    """ Must be added before Generic View"""

    def get_language(self):
        if self.request.user.is_anonymous:
            try:
                language = self.request.session.get('language')
            except AttributeError:
                language = 0
        else:
            try:
                language = self.request.user.site_language.id
            except AttributeError:
                language = None
        return language

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["language"] = self.get_language()
        return context


class StaffPanelView(UserPassesTestMixin, TemplateView):
    template_name = 'staff_panel.html'

    def test_func(self):
        permitted = False
        if self.request.user.is_admin or self.request.user.is_staff:
            permitted = True
        return permitted


class HomePageView(TemplateView):
    template_name = "home.html"

    def greeting(self):
        # Translators: This message appears on the home page only
        output = _(
            'Today is %(month)s %(day)s. %(question)s'
        ) % {
            'month': "July",
            'day': "8",
            'question': WeeklyQuestion.questions.current().text,
        }
        return output

    def weekly(self):
        return WeeklyQuestion.questions.current()

    def language(self):
        return self.request.COOKIES.get("django_language")


class AboutView(TemplateView):
    template_name = "about.html"


class SurveyLandingPage(TemplateView):
    template_name = "survey_landing.html"


class ListenSignupView(CreateView):
    template_name = 'listen_signup.html'
    success_url = reverse_lazy('dashboard')
    form_class = CustomUserCreationForm
    model = ListenUser

    def headline(self):
        return Copy.objects.get(location=choices.SIGN_UP_HEADLINE)

    def bullets(self):
        return Copy.objects.filter(location=choices.SIGN_UP_BULLET)

    def paragraphs(self):
        return Copy.objects.filter(location=choices.SIGN_UP_P)


class ListenLoginView(LoginView):
    template_name = 'listen_login.html'


def logout_request(request):
    logout(request)
    return redirect(reverse_lazy('home'))


class ListenPWResetView(LanguageMixin, TemplateView):
    template_name = 'listen_pw_reset.html'


class NewsView(LanguageMixin, TemplateView):
    template_name = 'news.html'

    def blog_post(self):
        return Post.objects.filter(blog__organization=1, status=1).latest('published_on')

    def recent_newsletters(self):
        newsletters = list(Message.objects.filter(newsletter__slug='shafter-listener', submission__sent=True))[::-1][:5]
        most_recent = newsletters.pop(0)
        return [most_recent, newsletters]


class DonateView(LanguageMixin, TemplateView):
    template_name = 'donate.html'

    def bullets(self):
        return Copy.objects.filter(location=choices.DONATE_BULLET)

    def paragraphs(self):
        return Copy.objects.filter(location=choices.DONATE_P)


class DashboardView(LoginRequiredMixin, LanguageMixin, TemplateView):
    template_name = 'dashboard.html'

    def get_initial(self):
        user = self.request.user
        initial = {
            'email': user.email,
            'phone_number': user.phone_number,
            'allows_contact_email': user.allows_contact_email,
            'allows_contact_sms': user.allows_contact_sms
        }
        return initial
    #
    # def get_form(self, form_class=None):
    #     form_class = self.get_form_class()
    #     return form_class(instance=self.request.user, **self.get_form_kwargs())

    def get_success_url(self):
        return self.request.path

    def form_valid(self, form):
        form.save()
        return super(DashboardView, self).form_valid(form)

    def new_surveys(self):
        respondent = Respondent.objects.get(user=self.request.user)
        surveys = Questionnaire.available.today().filter(eligible_users=respondent)
        surveys = surveys.exclude(
            id__in=respondent.started.all() | respondent.finished.all()
        )
        return surveys

    def started(self):
        respondent = Respondent.objects.get(user=self.request.user)
        surveys = Questionnaire.available.today().filter(eligible_users=respondent, id__in=respondent.started.all())
        surveys = surveys.exclude(id__in=respondent.finished.all())
        return surveys

    def finished(self):
        respondent = Respondent.objects.get(user=self.request.user)
        surveys = Questionnaire.available.today().filter(eligible_users=respondent, id__in=respondent.finished.all())
        return surveys


class AdminToolsView(UserPassesTestMixin, TemplateView):
    template_name = 'admin_tools.html'

    def test_func(self):
        return self.request.user.is_admin

    def researchers(self):
        return Researcher.objects.filter(user__organization=self.request.user.organization)

    def user_accounts(self):
        accounts = list(
            ListenUser.objects.filter(
                organization=self.request.user.organization
            ).values_list(
                'username', flat=True)
        )

        return accounts


def create_articles(message_obj):
    Article.objects.create(
        title='intro',
        text="Insert Text",
        post=message_obj,
    )
    Article.objects.create(
        title='first-img',
        text="https://support.hostgator.com/img/articles/weebly_image_sample.png",
        post=message_obj
    )
    Article.objects.create(
        title='second-img',
        text="https://support.hostgator.com/img/articles/weebly_image_sample.png",
        post=message_obj
    )

    for i in range(2):
        Article.objects.create(
            title='second',
            text="Insert Text",
            post=message_obj,
        )

    for i in range(3):
        Article.objects.create(
            title='first',
            text="Insert Text",
            post=message_obj,
        )

        Article.objects.create(
            title='third',
            text="Insert Text",
            post=message_obj,
        )


class NewsletterBuilderView(UserPassesTestMixin, LanguageMixin, TemplateView):
    template_name = "newsletter_builder.html"

    def test_func(self):
        permitted = False
        if self.request.user.is_admin or self.request.user.is_staff:
            permitted = True
        return permitted

    def date_str(self):
        components = self.kwargs.get('slug').split('-')
        month = components[0].capitalize()
        year = components[1]
        return "{} {}".format(month, year)

    def message_id(self):
        slug = self.kwargs.get('slug')
        message_obj = Message.objects.filter(slug=slug).first()
        if not message_obj:
            newsletter = Newsletter.objects.get(slug='shafter-listener')
            message_obj = Message.objects.create(
                title=self.date_str(),
                slug=slug,
                newsletter=newsletter,
            )
            create_articles(message_obj)
        return message_obj.id


class ViewNewsletter(TemplateView):
    template_name = 'read_newsletter.html'

    def contents(self):
        slug = self.kwargs.get('slug')
        message = Message.objects.get(slug=slug)
        output = {
            'title': message.title,
            'intro': Article.objects.get(post=message, title='intro').text,
            'firstImg': Article.objects.get(post=message, title='first-img').text,
            'secondImg': Article.objects.get(post=message, title='second-img').text,
        }
        for section in ['first', 'second', 'third']:
            articles = Article.objects.filter(post=message, title=section)
            output[section] = [article.text for article in articles]
        return output


class NewsletterListView(TemplateView):
    template_name = 'newsletter_list.html'

    def messages(self):
        messages = Message.objects.filter(newsletter__slug='shafter-listener').order_by('-date_create')
        print(messages)
        output = {}
        for message in messages:
            year = message.date_create.year
            data = {'slug': message.slug, 'title': message.title}
            if year in output:
                output[year].append(data)
            else:
                output[year] = [data]
        return output


class SnapcodeView(LanguageMixin, TemplateView):
    template_name = 'snapcode.html'


class FoodSecurityView(LanguageMixin, TemplateView):
    template_name = 'food_security.html'

