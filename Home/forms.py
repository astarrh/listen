from Home.models import ListenUser, Whisper
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.forms import ModelForm
#
# class ListenUserAdapter(DefaultAccountAdapter):
#     def save_user(self, request, user, form, commit=False):
#         user = super(ListenUserAdapter, self).save_user(request, user, form, commit=commit)
#         user_input = request.POST.get('allows_contact')
#         user.allows_contact_email = user.allows_contact_sms = True if user_input == "on" else False
#         user.organization, _ = Organization.objects.get_or_create(host=request.get_host())
#         if "@" is user.username:
#             user.login_is_email = True
#         user.save()


LABELS = {
    'allows_contact_email': "Allow Contact via Email",
    'allows_contact_sms': "Allow Contact via Text/SMS",
    'phone_number': "Phone Number",
    'email': "Email"
}


def get_label_translations():
    labels = LABELS
    for index, label in labels.items():
        try:
            labels[index] = TranslatedFixture.objects.get(original_text=label).translated_text
        except TranslatedFixture.DoesNotExist:
            pass
    return labels


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = ListenUser
        fields = ['username']


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = ListenUser
        fields = ['username']


class UserSettingsForm(ModelForm):

    class Meta:
        model = ListenUser
        fields = ['allows_contact_email', 'allows_contact_sms', 'phone_number', 'email']
        labels = LABELS



class SecretForm(ModelForm):

    class Meta:
        model = Whisper
        fields = ['data',]
