import re
from io import open

from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from newsletter.models import Message, Submission, Subscription, Newsletter, Article

from Analyze.models import Researcher
from .models import Language, Questionnaire, ListenUser, Organization
from .forms import SecretForm

def quick_signup(request):
    response = {'success': False}
    email = request.POST.get("email")
    password = request.POST.get('password')
    try:
        ListenUser.objects.create_user(username=email, password=password)
        response = {'success': True}
    except ValidationError:
        response["error"] = "Please use a longer password."
    return JsonResponse(response)


def submit_secret(request):
    response = {'success': False}
    if request.method != "POST":
        return JsonResponse(response)

    form = SecretForm(request.POST)
    if form.is_valid():
        form.save()
    response = {'success': True}
    return JsonResponse(response)


def change_language(request):
    response = {'success': False}
    if request.method != "POST":
        return JsonResponse(response)

    spanish = Language.objects.get(label="Spanish")

    if request.user.is_anonymous:
        try:
            prev_language = request.session.get('language')
            if prev_language:
                request.session['language'] = None
            else:
                request.session['language'] = spanish.id
        except AttributeError:
            request.session['language'] = spanish.id
    else:
        if request.POST.get('data') == 'site':
            if request.user.site_language:
                request.user.site_language = request.user.survey_language = None
            else:
                request.user.site_language = request.user.survey_language = spanish
        else:
            if request.user.site_language:
                request.user.survey_language = None
            else:
                request.user.survey_language = spanish
        request.user.save()

    response = {'success': True}
    return JsonResponse(response)


def set_permissions(request):
    response = {'success': False}
    if request.method != "POST":
        return JsonResponse(response)

    survey = Questionnaire.objects.get(id=request.POST.get('survey'))
    researcher_pks = request.POST.get('dataStr').lstrip(',').split(',')
    for pk in researcher_pks:
        Researcher.objects.get(id=pk).eligible_for.add(survey)

    response = {'success': True}
    return JsonResponse(response)


def modify_newsletter(request):
    data = request.POST
    issue = Message.objects.get(id=data.get('messageID'))
    structure = data.get('section')

    print(structure)
    if "intro" in structure:
        section = "intro"
        index = 0
    elif "img" in structure:
        section = structure
        index = 0
    else:
        structure = structure.split('-')
        section = structure[0]
        index = structure[1]

    text = data.get('text')
    print(text)
    article = Article.objects.filter(post=issue, title=section).order_by('id')[int(index)]
    article.text = text
    article.save()

    response = {'success': True}
    return JsonResponse(response)


def setup_newsletter(slug, resend=False):
    subs = []
    emails = []
    organization = Organization.objects.get(name="Listen To Shafter")
    users = ListenUser.objects.filter(organization=organization, email__isnull=False, allows_contact_email=True)
    newsletter = Newsletter.objects.get(slug="shafter-listener")

    if resend is True:
        addresses = []
        for line in open('newsletter/failures.csv', newline='\r\n'):
            match = re.search(r'[\w\.-]+@[\w\.-]+', line)
            addresses.append(match.group(0))
        for address in addresses:
            try:
                sub = Subscription.objects.get(email_field=address)
                if address not in emails:
                    subs.append(sub)
            except ObjectDoesNotExist:
                sub = Subscription.objects.create(
                    newsletter=newsletter,
                    email_field=address,
                    subscribed=True
                )
                subs.append(sub)
    else:
        for user in users:
            try:
                sub = Subscription.objects.get(user=user)
                subs.append(sub)
            except ObjectDoesNotExist:
                if user.email:
                    sub = Subscription.objects.create(
                        user=user,
                        name=user.username,
                        newsletter=newsletter,
                        subscribed=True
                    )
                    emails.append(user.email)
                    subs.append(sub)

        for line in open('newsletter/addresses.csv', newline='\n'):
            email = line.rstrip('\r\n')
            try:
                sub = Subscription.objects.get(email_field=email)
                if email not in emails:
                    subs.append(sub)
            except ObjectDoesNotExist:
                sub = Subscription.objects.create(
                    newsletter=newsletter,
                    email_field=email,
                    subscribed=True
                )
                subs.append(sub)

    message = Message.objects.get(slug=slug)

    submission = Submission.objects.create(
        message=message,
        publish=True
    )

    submission.subscriptions.add(*subs)
