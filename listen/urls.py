from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView

from Home import views as views
from Home import actions as actions

from Project import views as projects_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('tinymce/', include('tinymce.urls')),

    path('newsletter/', include('newsletter.urls')),
    path('surveys/', include(('Survey.urls', 'Survey'), namespace="Surveys")),
    path('blog/', include(('Blog.urls', "Blog"), namespace="Blog")),
    path('projects/', include(('Project.urls', "Project"), namespace="Project")),
    path('analysis/', include(('Analyze.urls', 'Analyze'), namespace='Analyze')),
    path('contacts/', include(('Contacts.urls', 'Contacts'), namespace='Contacts')),

    path('admin_tools/', views.AdminToolsView.as_view(), name="admin_tools"),

    path('accounts/login/', views.ListenLoginView.as_view(), name='account_login'),
    path('accounts/logout/', views.logout_request, name='account_logout'),
    path('accounts/signup/', views.ListenSignupView.as_view(), name='account_signup'),
    path('accounts/', include('django.contrib.auth.urls')),

    path('', views.HomePageView.as_view()),
    path('contribute/', views.DonateView.as_view(), name='donate'),
    path('dashboard/', views.DashboardView.as_view(), name="dashboard"),
    path('snapcode/', views.SnapcodeView.as_view(), name='snapcode'),
    path('news/', views.NewsView.as_view(), name='news'),

    path('newsletter_builder/<slug:slug>/', views.NewsletterBuilderView.as_view(), name='newsletter_builder'),
    path('read_newsletter/<slug:slug>/', views.ViewNewsletter.as_view(), name='view_newsletter'),
    path('newsletters/', views.NewsletterListView.as_view(), name='newsletter_list'),
    path('modify_newsletter/', actions.modify_newsletter, name='modify_newsletter'),

    path('submit_secret/', actions.submit_secret),
    path('set_permissions/', actions.set_permissions),
    path('quick_signup/', actions.quick_signup, name='quick_signup'),

    path('email_copy/', TemplateView.as_view(template_name='email_copy.html'), name='email'),


    path('about/', views.AboutView.as_view(), name="about"),
    path('start_survey/', views.SurveyLandingPage.as_view(), name="survey_landing"),
    path('home/', views.HomePageView.as_view(), name="home"),
    path('staff/', views.StaffPanelView.as_view(), name='staff'),

    path('i18n/', include('django.conf.urls.i18n')),
    path('', views.HomePageView.as_view()),

    path('sample/', projects_views.SampleProjectView.as_view(), name='sample'),
    path('<slug:slug>/', projects_views.ProjectView.as_view(), name='project'),
]


